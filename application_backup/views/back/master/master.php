<?php $this->load->view('back/template/meta'); ?>
<?php $this->load->view('back/template/header'); ?>
<?php $this->load->view('back/template/sidebar'); ?>

<div class="main-panel">
<!-- BEGIN : Main Content-->
    <div class="main-content">
        <div class="content-wrapper">

	<!--Statistics cards Starts-->
		<div class="row">   
          <div class="col-xl-2 col-lg-6 col-12">
              <div class="card-content">
                <br>
                <div class="px-5 py-3">
                  <div class="media">
                    <div class="media-body text-left">
                      <h4 class="mb-1 Success"><b>PROGRAM</b></h4>
                    </div>
                  </div>
                </div>
              </div>
          </div>
          <div class="col-xl-1 col-lg-6 col-12" >
          <div class="card">
            <div class="media-center align-self-center">
              <br>
              <i class="icon-user green font-large-1 float-left"></i>
            </div>
                <div class="px-3 py-1">
                  <div class="media">
                    <div class="media-body text-left">
                      <h6 class="mb-1 green">Lamusta</h6>
                      <span></span>
                    </div>
                  </div>
            </div>
          </div>
          </div>
          <div class="col-xl-1 col-lg-6 col-12" >
          <div class="card">
            <div class="media-center align-self-center">
              <br>
              <i class="icon-umbrella green font-large-1 float-left"></i>
            </div>
                <div class="px-4 py-1">
                  <div class="media">
                    <div class="media-body text-left">
                      <h6 class="mb-1 green">PKM</h6>
                      <span></span>
                    </div>
                  </div>
            </div>
          </div>
          </div>
          <div class="col-xl-1 col-lg-6 col-12" >
          <div class="card">
            <div class="media-center align-self-center">
              <br>
              <i class="icon-bulb green font-large-1 float-left"></i>
            </div>
                <div class="px-4 py-1">
                  <div class="media">
                    <div class="media-body text-left">
                      <h6 class="mb-1 green">YTB</h6>
                      <span></span>
                    </div>
                  </div>
            </div>
          </div>
          </div>
          <div class="col-xl-1 col-lg-6 col-12" >
          <div class="card">
            <div class="media-center align-self-center">
              <br>
              <i class="icon-shuffle green font-large-1 float-left"></i>
            </div>
                <div class="px-3 py-1">
                  <div class="media">
                    <div class="media-body text-left">
                      <h6 class="mb-1 green">Shelter</h6>
                      <span></span>
                    </div>
                  </div>
            </div>
          </div>
          </div>
          <div class="col-xl-1 col-lg-6 col-12" >
          <div class="card">
            <div class="media-center align-self-center">
              <br>
              <i class="icon-puzzle green font-large-1 float-left"></i>
            </div>
                <div class="px-4 py-1">
                  <div class="media">
                    <div class="media-body text-left">
                      <h6 class="mb-1 green">BSL</h6>
                      <span></span>
                    </div>
                  </div>
            </div>
          </div>
          </div>
          <div class="col-xl-1 col-lg-6 col-12" >
          <div class="card">
            <div class="media-center align-self-center">
              <br>
              <i class="icon-present green font-large-1 float-left"></i>
            </div>
                <div class="px-4 py-1">
                  <div class="media">
                    <div class="media-body text-left">
                      <h6 class="mb-1 green">BRP</h6>
                      <span></span>
                    </div>
                  </div>
            </div>
          </div>
          </div>

          <div class="col-xl-1 col-lg-6 col-12" >
          <div class="card">
            <div class="media-center align-self-center">
              <br>
              <i class="icon-badge green font-large-1 float-left"></i>
            </div>
                <div class="px-4 py-1">
                  <div class="media">
                    <div class="media-body text-left">
                      <h6 class="mb-1 green">PDM</h6>
                      <span></span>
                    </div>
                  </div>
            </div>
          </div>
          </div>
          <div class="col-xl-1 col-lg-6 col-12" >
          <div class="card">
            <div class="media-center align-self-center">
              <br>
              <i class="icon-cup green font-large-1 float-left"></i>
            </div>
                <div class="px-4 py-1">
                  <div class="media">
                    <div class="media-body text-left">
                      <h6 class="mb-1 green">Darling</h6>
                      <span></span>
                    </div>
                  </div>
            </div>
          </div>
          </div>

          <div class="col-xl-1 col-lg-6 col-12" >
          <div class="card">
            <div class="media-center align-self-center">
              <br>
              <i class="icon-directions green font-large-1 float-left"></i>
            </div>
                <div class="px-4 py-1">
                  <div class="media">
                    <div class="media-body text-left">
                      <h6 class="mb-1 green">Barzah</h6>
                      <span></span>
                    </div>
                  </div>
            </div>
          </div>
          </div>        
          </div>
        <!--Statistics cards Ends-->
		<div class="row match-height">
  <div class="col-xl-4 col-lg-12 col-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Cari Data</h4>
        <br>
      </div>
      <div class="card-content">
      <div class="px-4">        
      <button type="button" data-toggle="collapse" class="navbar-toggle d-lg-none float-left"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><span class="d-lg-none navbar-right navbar-collapse-toggle"><a aria-controls="navbarSupportedContent" href="javascript:;" class="open-navbar-container black"><i class="ft-more-vertical"></i></a></span>
      Individu      
        <form role="search">
              <div class="position-relative has-icon-right">
                <input type="text" placeholder="Cari Berdasarkan NIK" class="form-control round" id="caripm"/>
                <div class="form-control-position"><i class="ft-search"></i></div>
              </div>
        </form>
      </div>        
      </div>
      <br>
      <div class="card-content">
      <div class="px-4">        
      <button type="button" data-toggle="collapse" class="navbar-toggle d-lg-none float-left"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><span class="d-lg-none navbar-right navbar-collapse-toggle"><a aria-controls="navbarSupportedContent" href="javascript:;" class="open-navbar-container black"><i class="ft-more-vertical"></i></a></span>
      Komunitas    
        <form role="search">
              <div class="position-relative has-icon-right">
                <input type="text" placeholder="Cari Berdasarkan NIK" class="form-control round" id="caripm"/>
                <div class="form-control-position"><i class="ft-search"></i></div>
              </div>
        </form>
      </div>        
      </div> 
    </div>
  </div>
  <div class="col-xl-8 col-lg-12 col-12">
    <div class="card">
      <div class="card-header pb-2">
        <h4 class="card-title">DATA PM (Individu/Komunitas)</h4>
      </div>
      <div class="card-content">
        <table class="table table-responsive-sm text-center">
          <thead>
            <tr>
              <th>Foto</th>
              <th>NIK</th>
              <th>Nama</th>
              <th>Status</th>
              <th>Amount</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><img class="media-object round-media height-50" src="app-assets/img/elements/01.png" alt="Generic placeholder image" /></td>
              <td>Ferrero Rocher</td>
              <td>1</td>
              <td>
                <a class="btn white btn-round btn-primary">Active</a>
              </td>
              <td>$19.94</td>
              <td>
                <a class="danger" data-original-title="" title="">
                  <i class="ft-x"></i>
                </a>
              </td>
            </tr>
            <tr>
              <td><img class="media-object round-media height-50" src="app-assets/img/elements/07.png" alt="Generic placeholder image" /></td>
              <td>Headphones</td>
              <td>2</td>
              <td>
                <a class="btn white btn-round btn-danger">Disabled</a>
              </td>
              <td>$99.00</td>
              <td>
                <a class="danger" data-original-title="" title="">
                  <i class="ft-x"></i>
                </a>
              </td>
            </tr>
            <tr>
              <td><img class="media-object round-media height-50" src="app-assets/img/elements/11.png" alt="Generic placeholder image" /></td>
              <td>Camera</td>
              <td>1</td>
              <td>
                <a class="btn white btn-round btn-info">Paused</a>
              </td>
              <td>$299.00</td>
              <td>
                <a class="danger" data-original-title="" title="">
                  <i class="ft-x"></i>
                </a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>		
<div class="row match-height">
  <div class="col-xl-10 col-lg-12 col-12">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title mb-0">Program</h4>
      </div>
      <div class="card-content">
        <div class="card-body">
          <table class="table table-striped table-bordered zero-configuration">
              <thead>
                <tr>
                  <th>Direktorat</th>
                  <th>Divisi</th>
                  <th>Organ</th>
                  <th>Bidang Kegiatan</th>
                  <th>Nama Program</th>
                  <th>Bentuk Manfaat</th>
                  <th>Jml. PM</th>
                  <th>Satuan</th>
                  <th>Asnaf</th>
                  <th>Sumber Dana</th>
                  <th>Periode Program</th>
                  <th>Keterangan</th>
                </tr>
              </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-2 col-lg-12 col-12">
    <div class="card gradient-blackberry">
      <div class="card-content">
        <div class="card-body">
          <h4 class="card-title white">Total PM</h4>
          <div class="my-3 text-center white">
            <a class="font-large-2 d-block mb-1"> 78.89</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="page-inner">
<?php $this->load->view('back/dashboard/record'); ?>
</div>

  	
</div>
</div>
<?php $this->load->view('back/template/footer'); ?>
