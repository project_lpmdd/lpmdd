<!-- total data -->
<!-- <h4 class="page-title">Card</h4> -->
<div class="row">
  <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3">
		<div class="card card-stats card-round">
			<div class="card-body">
				<div class="row align-items-center">
					<div class="col-icon">
						<div class="icon-big text-center icon-primary bubble-shadow-small">
							<i class="fa fa-list"></i>
						</div>
					</div>
					<div class="col col-stats ml-3 ml-sm-0">
						<div class="numbers">
							<p class="card-category"><label for="Menu">Menu</label></p>
							<h4 class="card-title"><?php echo $get_total_menu ?></h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
  <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3">
		<div class="card card-stats card-round">
			<div class="card-body">
				<div class="row align-items-center">
					<div class="col-icon">
						<div class="icon-big text-center icon-warning bubble-shadow-small">
							<i class="fa fa-list"></i>
						</div>
					</div>
					<div class="col col-stats ml-3 ml-sm-0">
						<div class="numbers">
							<p class="card-category"><label for="SubMenu">SubMenu</label> </p>
							<h4 class="card-title"><?php echo $get_total_submenu ?></h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
  <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3">
		<div class="card card-stats card-round">
			<div class="card-body">
				<div class="row align-items-center">
					<div class="col-icon">
						<div class="icon-big text-center icon-success bubble-shadow-small">
							<i class="fa fa-universal-access"></i>
						</div>
					</div>
					<div class="col col-stats ml-3 ml-sm-0">
						<div class="numbers">
							<p class="card-category"><label for="Usertype">Usertype</label></p>
							<h4 class="card-title"><?php echo $get_total_usertype ?></h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
  <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3">
		<div class="card card-stats card-round">
			<div class="card-body">
				<div class="row align-items-center">
					<div class="col-icon">
						<div class="icon-big text-center icon-danger bubble-shadow-small">
							<i class="fa fa-users"></i>
						</div>
					</div>
					<div class="col col-stats ml-3 ml-sm-0">
						<div class="numbers">
							<p class="card-category"><label for="User">User</label> </p>
							<h4 class="card-title"><?php echo $get_total_user ?></h4>
						</div>
					</div>
				</div>
			</div>
		</div>
  </div>
</div>
