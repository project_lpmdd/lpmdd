<!DOCTYPE html>
<html lang="en" class="loading">
<!-- BEGIN : Head-->

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Project 2022">
  <meta name="keywords" content="Daskim, dompet dhuafa, lpm, lembaga, pelayan, masyarakat, dd, ciputat">
  <meta name="author" content="Daskim">
  <title>LPM DD</title>
  <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('app-assets/') ?>img/ico/apple-icon-60.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('app-assets/') ?>img/ico/apple-icon-76.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('app-assets/') ?>img/ico/apple-icon-120.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('app-assets/') ?>img/ico/apple-icon-152.png">
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('app-assets/') ?>img/ico/favicon.ico">
  <link rel="shortcut icon" type="image/png" href="<?php echo base_url('app-assets/') ?>img/ico/favicon-32.png">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-touch-fullscreen" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="default">
  <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900|Montserrat:300,400,500,600,700,800,900" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url('app-assets/') ?>fonts/feather/style.min.css">
  <link rel="stylesheet" href="<?php echo base_url('app-assets/') ?>fonts/simple-line-icons/style.css">
  <link rel="stylesheet" href="<?php echo base_url('app-assets/') ?>/fonts/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url('app-assets/') ?>vendors/css/perfect-scrollbar.min.css">
  <link rel="stylesheet" href="<?php echo base_url('app-assets/') ?>vendors/css/prism.min.css">
  <link rel="stylesheet" href="<?php echo base_url('app-assets/') ?>vendors/css/chartist.min.css">
  <link rel="stylesheet" href="<?php echo base_url('app-assets/') ?>css/app.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/') ?>select2/dist/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo base_url('app-assets/') ?>vendors/css/tables/datatable/datatables.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="https://glyphsearch.com/bower_components/ionicons/css/ionicons.css">
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</head>
<!-- END : Head-->