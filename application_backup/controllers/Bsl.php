<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bsl extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();

    $this->data['module'] = 'Bsl';

    $this->load->model(array('Bsl_model'));

    $this->load->helper(array('url', 'html'));
    $this->load->database();

    // company_profile
    $this->data['company_data']          = $this->Company_model->company_profile();

    // template
    $this->data['logo_header_template']  = $this->Template_model->logo_header();
    $this->data['navbar_template']       = $this->Template_model->navbar();
    $this->data['sidebar_template']      = $this->Template_model->sidebar();
    $this->data['background_template']   = $this->Template_model->background();
    $this->data['sidebarstyle_template'] = $this->Template_model->sidebarstyle();

    $this->data['btn_submit'] = '<button type="submit" name="button" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>';
    $this->data['btn_reset']  = 'Bersih';
    $this->data['btn_back']   = '<button type="button" onclick="history.back()" class="btn btn-secondary"> Kembali</button>';
    $this->data['btn_add']    = 'Add New Data';
    $this->data['add_action'] = base_url('lamustaindividu/create');
  }

  function konselor()
  {
    is_login();
    is_read();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title'] = $this->data['module'] . ' List';
    $this->data['header']     = $this->data['module'];
    $this->data['konselor']   = $this->Bsl_model->get_all_individu();

    $data['path'] = base_url('assets');
    $this->load->view('back/bsl/konselor/konselor_list', $this->data);
  }

  function create($id)
  {
    is_login();
    is_create();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title']       = 'Create New Manfaat BSL';
    $this->data['header']           =  $this->data['module'];
    $this->data['action_individu']  = 'bsl/create_action_individu';
    $this->data['action_komunitas'] = 'bsl/create_action_komunitas';

    $this->data['data']             = $this->Bsl_model->get_data_individu($id);
    // $this->data['komunitas']        = $this->Bsl_model->get_data_komunitas($id);
    $this->data['komunitas']        = $this->Bsl_model->get_data_komunitas_by_program();
    $this->data['program']           = $this->Bsl_model->get_program();
    $this->data['subprogram']       = $this->Bsl_model->get_sub_program();
    $this->data['cek_individu']     = $this->Bsl_model->check_transaksi_individu_by_id($id);
    $this->data['cek_komunitas']    = $this->Bsl_model->check_transaksi_komunitas_by_id($id);
    $this->data['notif_individu']   = $this->Bsl_model->get_transaksi_individu_by_date($id);
    $this->data['notif_komunitas']  = $this->Bsl_model->get_transaksi_komunitas_by_date($id);
    $this->data['rekomender']       = $this->Bsl_model->get_rekomender();
    $this->data['datapenduduk']       = $this->Bsl_model->get_datapenduduk();
    $this->data['jk'] = [
      'name'          => 'jk',
      'id'            => 'jk',
      'class'         => 'form-control',
      'autocomplete'  => 'off',
      // 'required'      => '',
      'value'         => $this->form_validation->set_value('jk'),
    ];
    $this->data['jk_value'] = [
      ''              => 'Tidak ada keterangan',
      'L'             => 'Laki-laki',
      'P'             => 'Perempuan',
    ];

    // var_dump($this->Bsl_model->get_datapenduduk());die;
    $this->load->view('back/bsl/konselor/konselor_add', $this->data);
  }

  function create_action_individu()
  {
    $data = array(
      'id_individu' => $this->input->post('id_individu'),
      'nik' => $this->input->post('nik'),
      'nama' => $this->input->post('nama'),
      'id_program' => $this->input->post('id_program'),
      'id_subprogram' => $this->input->post('id_subprogram'),
      'info_bantuan' => $this->input->post('info_bantuan'),

      'asnaf' => $this->input->post('asnaf'),
      'status_tinggal' => "",
      'kasus' => "",
      'rekomendasi_bantuan' => "",
      'rencana_bantuan' => "",
      'mekanisme_bantuan' => "",
      'jumlah_bantuan' => str_replace(",", "", $this->input->post('jumlah_bantuan')),
      'jumlah_permohonan' => str_replace(",", "", $this->input->post('jumlah_permohonan')),
      'jenis_bantuan' => $this->input->post('jenis_bantuan'),
      'sifat_bantuan' => "",
      'bidang_tugas' => $this->input->post('bidang_tugas'),
      'profesi' => $this->input->post('profesi'),
      'kopentensi' => $this->input->post('kopentensi'),
      'sumber_dana'         => $this->input->post('sumber_dana'),
      'periode_bantuan'         => $this->input->post('periode_bantuan'),
      'total_periode'         => $this->input->post('total_periode'),

      'deleted_at'         => '0000-00-00 00:00:00',
      'created_by'         => $this->session->username
    );

    $this->Bsl_model->insert($data);
    write_log();
    $this->session->set_flashdata('message', '<div class="alert alert-success">Data saved succesfully</div>');
    redirect(base_url('bsl/konselor'));
  }

  function update_individu($id)
  {
    is_login();
    is_update();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title']  = 'Update Data ' . $this->data['module'];
    $this->data['update_action_individu'] = 'bsl/update_action_individu';

    $this->data['data']        = $this->Bsl_model->get_transaksi_individu_update($id);
    $this->data['rekomender']  = $this->Bsl_model->get_rekomender();
    $this->data['subprogram']  = $this->Bsl_model->get_sub_program();

    if ($this->data['data']) {
      $this->load->view('back/bsl/konselor/konselor_edit_individu', $this->data);
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">Data not found</div>');
      redirect('bsl/detail_konselor/' . $this->data['data'][0]['nik']);
    }
  }

  function update_action_individu()
  {
    $data = array(
      'id_individu' => $this->input->post('id_individu'),
      'nik' => $this->input->post('nik'),
      'nama' => $this->input->post('nama'),
      'id_program' => $this->input->post('id_program'),
      'id_subprogram' => $this->input->post('id_subprogram'),
      'info_bantuan' => $this->input->post('info_bantuan'),

      'asnaf' => $this->input->post('asnaf'),
      'status_tinggal' => "",
      'kasus' => "",
      'rekomendasi_bantuan' => "",
      'rencana_bantuan' => "",
      'mekanisme_bantuan' => "",
      'jumlah_bantuan' => str_replace(",", "", $this->input->post('jumlah_bantuan')),
      'jumlah_permohonan' => str_replace(",", "", $this->input->post('jumlah_permohonan')),
      'jenis_bantuan' => $this->input->post('jenis_bantuan'),
      'sifat_bantuan' => "",
      'bidang_tugas' => $this->input->post('bidang_tugas'),
      'profesi' => $this->input->post('profesi'),
      'kopentensi' => $this->input->post('kopentensi'),
      'sumber_dana'         => $this->input->post('sumber_dana'),
      'periode_bantuan'         => $this->input->post('periode_bantuan'),
      'total_periode'         => $this->input->post('total_periode'),

      'deleted_at'         => '0000-00-00 00:00:00',
      'created_by'         => $this->session->username
    );

    $where = $this->input->post('id_transaksi_individu');

    $this->Bsl_model->update($where, $data);

    write_log();
    $this->session->set_flashdata('message', '
        <div class="row mt-3">
          <div class="col-md-12">
            <div class="alert alert-success">Data update succesfully</div>
          </div>
        </div>
      ');
    redirect('bsl/detail_konselor/' . $this->input->post('nik'));
  }

  //----------------------- end lamusta individu --------------------------//


  function create_action_komunitas()
  {

    // var_dump($this->input->post('unik'));die;

    is_login();
    is_update();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title'] = 'Tambah Manfaat Lamusta';
    $this->data['header']     = $this->data['module'];
    $code  = $this->Bsl_model->autonumber();
    $number = 1;
    foreach ($this->input->post('Nama') as $key => $item_id) {
      $number += 1;
      $data = array(
        'id_trans_komunitas_h'  => $code,
        'nik'                   => "",
        'nama_det'                   => $this->input->post('ujenispasal')[$key],
        'jenispidana_pasal'     => $this->input->post('ujenispasal')[$key],
        'jenispidana_keterangan' => $this->input->post('uketerangan')[$key],
        'masahukuman'         => $this->input->post('umasahukuman')[$key],
        'blokkamar'          => $this->input->post('ublok')[$key],
        'jenis_kelamin '          => $this->input->post('jk')[$key],
        'alamat'          => $this->input->post('det_alamat')[$key]

      );
      $this->Bsl_model->insert_komunitasdetail($data);
    }
    $data = array(

      'id_komunitas'          => $this->input->post('id_komunitas'),
      'nik'                   => $this->input->post('nik'),
      'code'                  =>  $code,
      'nama'                  => $this->input->post('nama'),
      'id_program'            => $this->input->post('id_program'),
      'id_subprogram'         => $this->input->post('id_subprogram'),
      'info_bantuan'          => $this->input->post('info_bantuan'),
      'bentuk_manfaat'        => "",
      'asnaf'                 => $this->input->post('asnaf'),
      'jumlah_pm'             => $number,
      'rekomendasi_bantuan'   => "",
      'rencana_bantuan'       => "",
      'mekanisme_bantuan'     => "",
      // 'jumlah_permohonan'     => $this->input->post('jumlah_permohonan'),
      'jumlah_permohonan'     => "0",
      'jenis_bantuan'         => $this->input->post('jenis_bantuan'),
      'sifat_bantuan'         => "",
      'sumber_dana'         => $this->input->post('sumber_dana'),
      'total_periode' => $this->input->post('total_periode'),
      'periode_bantuan' => $this->input->post('periode'),
      'deleted_at'            => '0000-00-00 00:00:00',
      'created_by'            => $this->session->username
    );




    $this->Bsl_model->insert_komunitas($data);
    write_log();
    $this->session->set_flashdata('message', '<div class="alert alert-success">Data saved succesfully</div>');
    redirect(base_url('bsl/konselor'));
  }

  function update_komunitas($id)
  {
    is_login();
    is_update();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }
    // var_dump($this->Bsl_model->get_transaksi_komunitasdetail_update($id));die;
    $this->data['page_title']  = 'Update Data ' . $this->data['module'];
    $this->data['update_action_komunitas'] = 'bsl/update_action_komunitas';

    $this->data['data']          = $this->Bsl_model->get_transaksi_komunitas_update($id);
    $this->data['detail']          = $this->Bsl_model->get_transaksi_komunitasdetail_update($id);
    $this->data['komunitas']     = $this->Bsl_model->get_data_komunitas();
    $this->data['program']       = $this->Bsl_model->get_program();
    $this->data['rekomender']    = $this->Bsl_model->get_rekomender();
    $this->data['subprogram']    = $this->Bsl_model->get_sub_program();
    $this->data['jk'] = [
      'name'          => 'jk',
      'id'            => 'jk',
      'class'         => 'form-control',
      'autocomplete'  => 'off',
      // 'required'      => '',
      'value'         => $this->form_validation->set_value('jk'),
    ];
    $this->data['jk_value'] = [
      ''              => 'Tidak ada keterangan',
      'L'             => 'Laki-laki',
      'P'             => 'Perempuan',
    ];
    if ($this->data['data']) {
      $this->load->view('back/bsl/konselor/konselor_edit_komunitas', $this->data);
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">Data not found</div>');
      redirect('bsl/detail_konselor/' . $this->data['data'][0]['nik']);
    }
  }

  function update_action_komunitas()
  {
    is_login();
    is_update();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title'] = 'Edit BSL';
    $this->data['header']     = $this->data['module'];
    $jml = 1;
    foreach ($this->input->post('Nama') as $key => $item_id) {
      $jml  += 1;
      $iddet =  $this->input->post('uid')[$key];
      var_dump($iddet);
      die;
      $delete = $this->Bsl_model->get_by_id_detail($iddet);
      if ($delete) {
        $this->Bsl_model->deletedetail($iddet);
      }
      $data = array(
        'id_trans_komunitas_h'  => $iddet,
        'nik'                   => "",
        'nama_det'                   => $this->input->post('unama')[$key],
        'jenispidana_pasal'     => $this->input->post('ujenispasal')[$key],
        'jenispidana_keterangan' => $this->input->post('uketerangan')[$key],
        'masahukuman'         => $this->input->post('umasahukuman')[$key],
        'blokkamar'          => $this->input->post('ublok')[$key],
        'jenis_kelamin '          => $this->input->post('jk')[$key],
        'alamat'          => $this->input->post('det_alamat')[$key]

      );
      $this->Bsl_model->insert_komunitasdetail($data);
    }
    $data = array(
      'id_komunitas'          => $this->input->post('id_komunitas'),
      'nik'                   => $this->input->post('nik'),
      'code'                  =>  $iddet,
      'nama'                  => $this->input->post('nama'),
      'id_program'            => $this->input->post('id_program'),
      'id_subprogram'         => $this->input->post('id_subprogram'),
      'info_bantuan'          => $this->input->post('info_bantuan'),
      'bentuk_manfaat'        => "",
      'asnaf'                 => $this->input->post('asnaf'),
      'jumlah_pm'             => $jml,
      'rekomendasi_bantuan'   => "",
      'rencana_bantuan'       => "",
      'mekanisme_bantuan'     => "",
      'jumlah_permohonan'     => $this->input->post('jumlah_permohonan'),
      'jenis_bantuan'         => $this->input->post('jenis_bantuan'),
      'sifat_bantuan'         => "",
      'sumber_dana'         => $this->input->post('sumber_dana'),
      'total_periode' => $this->input->post('total_periode'),
      'periode_bantuan' => $this->input->post('periode'),
      'deleted_at'            => '0000-00-00 00:00:00',
      'created_by'            => $this->session->username
    );

    $where = $this->input->post('id_transaksi_komunitas');

    $this->Bsl_model->update_komunitas($where, $data);

    write_log();
    $this->session->set_flashdata('message', '
        <div class="row mt-3">
          <div class="col-md-12">
            <div class="alert alert-success">Data update succesfully</div>
          </div>
        </div>
      ');
    redirect('bsl/detail_konselor/' . $this->input->post('nik'));
  }

  function detail_konselor($id)
  {
    is_login();
    is_read();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title']        = 'Detail ' . $this->data['module'];
    $this->data['header']            = $this->data['module'];
    $this->data['get_all_provinsi']  = $this->Bsl_model->get_all_provinsi();
    $this->data['get_all_kabupaten'] = $this->Bsl_model->get_all_kabupaten();
    $this->data['get_all_kecamatan'] = $this->Bsl_model->get_all_kecamatan();
    $this->data['get_all_kelurahan'] = $this->Bsl_model->get_all_kelurahan();
    $this->data['penduduk']          = $this->Bsl_model->get_penduduk($id);


    $this->data['data_individu']     = $this->Bsl_model->get_transaksi_individu_by_id($id);
    $this->data['data_komunitas']    = $this->Bsl_model->get_transaksi_komunitas_by_id($id);
    $this->data['detail_individu']   = $this->Bsl_model->get_detail_individu_by_id($id);
    $this->data['detail_komunitas']  = $this->Bsl_model->get_detail_komunitas_by_id($id);

    $data['path'] = base_url('assets');
    $this->load->view('back/bsl/konselor/konselor_detail', $this->data);
  }

  function delete($id)
  {
    is_login();
    is_delete();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $delete = $this->Bsl_model->get_by_id($id);

    if ($delete) {
      $data = array(
        'is_delete'   => '1',
        'deleted_by'  => $this->session->username,
        'deleted_at'  => date('Y-m-d H:i:a'),
      );

      $this->Bsl_model->soft_delete($id, $data);

      // $this->write_log();

      $this->session->set_flashdata('message', '<div class="alert alert-success">Data deleted successfully</div>');
      redirect('bsl/detail_konselor');
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">No data found</div>');
      redirect('bsl/detail_konselor');
    }
  }

  function delete_individu($id)
  {
    is_login();
    is_delete();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $delete = $this->Bsl_model->delete_transaksi_individu_by_id($id);

    if ($delete) {
      $data = array(
        'is_delete'   => '1',
        'deleted_by'  => $this->session->username,
        'deleted_at'  => date('Y-m-d H:i:a'),
      );

      $this->Bsl_model->soft_delete($id, $data);

      $this->session->set_flashdata('message', '<div class="alert alert-success">Data deleted successfully</div>');
      redirect('bsl/detail_konselor/' . $delete->nik);
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">No data found</div>');
      redirect('bsl/detail_konselor' . $delete->nik);
    }
  }

  function delete_komunitas($id)
  {
    is_login();
    is_delete();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $delete = $this->Bsl_model->get_by_id_komunitas($id);

    if ($delete) {
      $data = array(
        'is_delete'   => '1',
        'deleted_by'  => $this->session->username,
        'deleted_at'  => date('Y-m-d H:i:a'),
      );

      $this->Bsl_model->soft_delete_komunitas($id, $data);

      // $this->write_log();

      $this->session->set_flashdata('message', '<div class="alert alert-success">Data deleted successfully</div>');

      redirect('bsl/detail_konselor' . $delete->nik);
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">No data found</div>');
      redirect('bsl/detail_konselor' . $delete->nik);
    }
  }

  function delete_permanent($id)
  {
    is_login();
    is_delete();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $delete = $this->Bsl_model->get_by_id($id);

    if ($delete) {
      $this->Bsl_model->delete($id);

      $this->session->set_flashdata('message', '<div class="alert alert-success">Data deleted permanently</div>');
      redirect('lamustaindividu/deleted_list');
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">No data found</div>');
      redirect('lamustaindividu/deleted_list');
    }
  }

  function deleted_list()
  {
    is_login();
    is_restore();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title'] = 'Deleted ' . $this->data['module'] . ' List';

    $this->data['komunitas'] = $this->Bsl_model->get_all_deleted_komunitas();
    $this->data['individu']  = $this->Bsl_model->get_all_deleted_individu();


    $this->load->view('back/bsl/individu_deleted_list', $this->data);
  }

  function restore($id)
  {
    is_login();
    is_restore();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $row = $this->Bsl_model->get_by_id($id);

    if ($row) {
      $data = array(
        'is_delete'   => '0',
        'deleted_by'  => NULL,
        'deleted_at'  => NULL,
      );

      $this->Bsl_model->update($id, $data);

      $this->session->set_flashdata('message', '<div class="alert alert-success">Data restored successfully</div>');
      redirect('lamustaindividu/deleted_list');
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">No data found</div>');
      redirect('lamustaindividu/deleted_list');
    }
  }

  function laporan_pusat()
  {
    is_login();
    is_read();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title']        = 'Data ' . $this->data['module'];
    $this->data['header']            = 'Laporan Konselor';
    $this->data['get_all_provinsi']  = $this->Bsl_model->get_all_provinsi();
    $this->data['get_all_kabupaten'] = $this->Bsl_model->get_all_kabupaten();
    $this->data['get_all_kecamatan'] = $this->Bsl_model->get_all_kecamatan();
    $this->data['get_all_kelurahan'] = $this->Bsl_model->get_all_kelurahan();
    $this->data['individu']          = $this->Bsl_model->get_all_admin_individu();
    $this->data['komunitas']         = $this->Bsl_model->get_laporan_komunitas();

    $data['path'] = base_url('assets');
    $this->load->view('back/bsl/laporan/konselor', $this->data);
  }

  function laporan_admin()
  {
    is_login();
    is_read();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title']        = 'Data ' . $this->data['module'];
    $this->data['header']            = 'Laporan Admin';
    $this->data['get_all_provinsi']  = $this->Bsl_model->get_all_provinsi();
    $this->data['get_all_kabupaten'] = $this->Bsl_model->get_all_kabupaten();
    $this->data['get_all_kecamatan'] = $this->Bsl_model->get_all_kecamatan();
    $this->data['get_all_kelurahan'] = $this->Bsl_model->get_all_kelurahan();
    $this->data['individu']          = $this->Bsl_model->get_all_admin_individu();
    $this->data['umur'] = [];
    for ($i = 0; $i < count($this->data['individu']); $i++) {
      $umur = $this->data['individu'][$i]->tgl_lahir;
      $res_umur = $this->hitung_umur($umur);
      array_push($this->data['umur'], $res_umur);
    }
    $this->data['komunitas']         = $this->Bsl_model->get_laporan_komunitas();

    $data['path'] = base_url('assets');
    $this->load->view('back/bsl/laporan/admin', $this->data);
  }

  function hitung_umur($tanggal_lahir)
  {
    $birthDate = new DateTime($tanggal_lahir);
    $today = new DateTime("today");
    if ($birthDate > $today) {
      exit("0 tahun 0 bulan 0 hari");
    }
    $y = $today->diff($birthDate)->y;
    $m = $today->diff($birthDate)->m;
    $d = $today->diff($birthDate)->d;
    return $y . " tahun " . $m . " bulan " . $d . " hari";
  }

  //------------------------[ DETAIL LAPORAN INDIVIDU & KOMUNITAS ]-----------------------------//
  function detail_laporan_individu($id)
  {
    is_login();
    is_update();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['get_all']      = $this->Bsl_model->get_laporan_individu_detail($id);

    if ($this->data['get_all']) {
      $this->data['page_title'] = 'Detail Data ' . $this->data['module'];
      $this->load->view('back/bsl/laporan/detail_laporan_individu', $this->data);
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">Penduduk not found</div>');
      redirect('bsl');
    }
  }

  // -------------- DETAIL LAPORAN KOMNITAS --------------
  function detail_laporan_komunitas($id)
  {
    is_login();
    is_update();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['get_all']      = $this->Bsl_model->get_laporan_komunitas_detail($id);

    if ($this->data['get_all']) {
      $this->data['page_title'] = 'Detail Data ' . $this->data['module'];
      $this->load->view('back/bsl/laporan/detail_laporan_komunitas', $this->data);
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">Penduduk not found</div>');
      redirect('bsl');
    }
  }
}
