<?php $this->load->view('back/template/meta'); ?>
<?php $this->load->view('back/template/header'); ?>
<?php $this->load->view('back/template/sidebar'); ?>

<div class="main-panel">
        <!-- BEGIN : Main Content-->
        <div class="main-content">
          <div class="content-wrapper"><!-- Basic Elements start -->
          <section class="basic-elements">
            <div class="row">
              <div class="col-sm-12">
                <div class="content-header"><?php echo $page_title ?></div>
              </div>
            </div>
            <div class="row match-height">
              <div class="col-md-12">
                <div class="card"><br>
                  <div class="card-content">
                    <?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');} ?>
                    <?php echo validation_errors(); ?>
                    <div class="px-3">
                      <?php echo form_open_multipart($action) ?>
                        <?php echo form_input($id_barzah, $barzah->id_barzah) ?>
                        <div class="form-body">
                          <div class="row">       

                            <div class="col-xl-6 col-lg-6 col-md-12 mb-1">
                              <fieldset class="form-group">
                                <p>No KTP</p>
                                <select class="form-control selectpicker text-capitalize" id="nik" name="nik">
                                  <?php foreach($get_penduduk as $row){
                                    if($barzah->nik == $row->nik){
                                      echo '<option value="'.$row->nik.'" selected >'.($row->nik).' - '.($row->nama).'</option>';
                                    }else{
                                      echo '<option value="'.$row->nik.'" >'.($row->nik).' - '.($row->nama).'</option>';
                                    }
                                  } ?>
                                </select>
                              </fieldset>
                            </div>

                            <div class="col-xl-6 col-lg-6 col-md-12 mb-1">
                              <fieldset class="form-group">
                                <p>Nama Jenazah</p>
                                <?php echo form_input($nama, $barzah->nama);?>
                              </fieldset>
                            </div>                     

                            <div class="col-xl-6 col-lg-6 col-md-12 mb-1">
                              <fieldset class="form-group">
                                <p>Sebab Kematian</p>
                                <?php echo form_input($sebab_kematian, $barzah->sebab_kematian);?>
                              </fieldset>
                            </div>

                            <div class="col-xl-6 col-lg-6 col-md-12 mb-1">
                              <fieldset class="form-group">
                                <p>Bentuk Layanan</p>
                                <?php echo form_input($bentuk_layanan, $barzah->bentuk_layanan);?>
                              </fieldset>                              
                            </div>

                            <div class="col-lg-6 col-md-12 mb-1">
                              <fieldset class="form-group">
                                <p>Tempat Meninggal</p>
                                <?php echo form_textarea($tempat_meninggal, $barzah->tempat_meninggal);?>                                
                              </fieldset>
                            </div>

                            <div class="col-xl-6 col-lg-12 col-md-12 mb-1">
                              <fieldset class="form-group">
                                <p>Meninggalkan</p>
                                <?php echo form_textarea($meninggalkan, $barzah->meninggalkan);?>
                              </fieldset>
                            </div>                          
                        </div>
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <?= $btn_submit ?>
                  <?php echo form_close() ?>
                  <?= $btn_back ?>
                </div>
              </div>
            </div>
          </section>
          <!-- Basic Inputs end -->
        </div>
      </div>
</div>
<?php $this->load->view('back/template/footer'); ?>

<script type="text/javascript">
    $(document).ready(function(){
       $('#nik').on('change',function(){
                var nik = $(this).val();
                $.ajax({
                    type : "GET",
                    url  : "<?= base_url('komunitas/get_data_penduduk')?>",
                    dataType : "JSON",
                    data : {nik: nik},
                    cache:false,
                    success: function(data){
                        $.each(data,function(nama){
                            $('#nama').val(data.nama);
                        });
                    }
                });
                return false;
           });
    });
  </script>
</body>
</html>


