<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Noreg extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->data['module'] = 'Noreg';

        $this->load->model(array('Noreg_model'));

        $this->load->helper(array('url', 'html'));
        $this->load->database();

        // company_profile
        $this->data['company_data']               = $this->Company_model->company_profile();

        // template
        $this->data['logo_header_template']       = $this->Template_model->logo_header();
        $this->data['navbar_template']            = $this->Template_model->navbar();
        $this->data['sidebar_template']           = $this->Template_model->sidebar();
        $this->data['background_template']        = $this->Template_model->background();
        $this->data['sidebarstyle_template']      = $this->Template_model->sidebarstyle();

        $this->data['btn_submit'] = '<button type="submit" name="button" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>';
        $this->data['btn_reset']  = 'Bersih';
        $this->data['btn_back']   = '<button type="button" onclick="history.back()" class="btn btn-success"> Kembali</button>';
        $this->data['btn_add']    = 'Add New Data';
        $this->data['add_action'] = base_url('Noreg/create');
    }

    function index()
    {
        is_login();
        is_read();

        if (!is_superadmin()) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
            redirect('dashboard');
        }

        $this->data['page_title'] = $this->data['module'] . ' List';
        $this->data['header'] = $this->data['module'];

        $this->data['konselor']  = $this->Noreg_model->get_all_individu();
        $this->data['individu']  = $this->Noreg_model->get_all_admin_individu();
        $this->data['komunitas'] = $this->Noreg_model->get_laporan_komunitas();

        $data['path'] = base_url('assets');

        $this->load->view('back/noreg/konselor/konselor_list', $this->data);
    }

    public function get_data_penduduk()
    {
        $nik  = $this->input->get('nik');
        $data = $this->Noreg_model->get_nik_penduduk($nik);
        echo json_encode($data);
    }

    public function get_data_Noreg()
    {
        $nama_Noreg  = $this->input->get('nama_Noreg');
        $data = $this->Noreg_model->get_data_Noreg($nama_Noreg);
        echo json_encode($data);
    }

    function create($id)
    {
        is_login();
        is_create();

        if (!is_superadmin()) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
            redirect('dashboard');
        }

        $this->data['page_title'] = 'Create New ' . $this->data['module'];
        $this->data['sub']        = "Tambah noreg";
        $this->data['header']     = $this->data['module'];
        $this->data['action_individu']  = "noreg/create_action_individu";
        $this->data['action_komunitas'] = 'noreg/create_action_komunitas';


        $this->data['get_all_provinsi']  = $this->Noreg_model->get_all_provinsi();
        $this->data['get_all_kabupaten'] = $this->Noreg_model->get_all_kabupaten();
        $this->data['get_all_kecamatan'] = $this->Noreg_model->get_all_kecamatan();
        $this->data['get_all_kelurahan'] = $this->Noreg_model->get_all_kelurahan();

        $this->data['data']             = $this->Noreg_model->get_data_individu($id);
        $this->data['komunitas']        = $this->Noreg_model->get_data_komunitas_by_program($id);
        $this->data['program']          = $this->Noreg_model->get_program();
        $this->data['subprogram']       = $this->Noreg_model->get_sub_program();
        $this->data['cek_individu']     = $this->Noreg_model->check_transaksi_individu_by_id($id);
        $this->data['cek_komunitas']    = $this->Noreg_model->check_transaksi_komunitas_by_id($id);
        $this->data['notif_individu']   = $this->Noreg_model->get_transaksi_individu_by_date($id);
        $this->data['notif_komunitas']  = $this->Noreg_model->get_transaksi_komunitas_by_date($id);
        $this->data['rekomender']       = $this->Noreg_model->get_rekomender();
        $this->data['datapenduduk']     = $this->Noreg_model->get_datapenduduk();
        $this->data['petugas'] = $this->Noreg_model->get_petugas();

        $this->load->view('back/noreg/noreg_add', $this->data);
    }

    function create_action_individu()
    {
        $this->form_validation->set_rules('nik', 'nik', 'required');
        $this->form_validation->set_rules('nama', 'nama', 'trim|required');

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

        if ($this->form_validation->run() === FALSE) {
            $this->create();
        } else {
            //insert ke table transaksi_individu
            $data2 = array(
                'id_individu'        => $this->input->post('id_individu'),
                'nik'                => $this->input->post('nik'),
                'nama'               => $this->input->post('nama'),
                'id_program'         => '10',
                'id_subprogram'      => $this->input->post('id_subprogram'),
                'jenis_bantuan'      => $this->input->post('jenis_bantuan'),
                'info_bantuan'       => $this->input->post('info_bantuan'),
                'asnaf'              => $this->input->post('asnaf'),
                'sumber_dana'        => $this->input->post('sumber_dana'),
                'periode_bantuan'    => $this->input->post('periode_bantuan'),
                'total_periode'      => $this->input->post('total_periode'),
                'jumlah_bantuan'      => $this->input->post('jumlah_bantuan'),
                'petugas'            => $this->input->post('petugas'),
                'deleted_at'         => '0000-00-00 00:00:00',
                'created_by'         => $this->session->username,
            );

            $this->Noreg_model->insert_individu($data2);
            write_log();

            $this->session->set_flashdata('message', '<div class="alert alert-success">Data saved succesfully</div>');
            redirect('noreg');
        }
    }

    function create_action_komunitas()
    {

        // var_dump($this->input->post('unik'));die;

        is_login();
        is_update();

        if (!is_superadmin()) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
            redirect('dashboard');
        }

        $this->data['page_title'] = 'Tambah Manfaat Noreg';
        $this->data['header']     = $this->data['module'];
        $code  = $this->Noreg_model->autonumber();
        $number = 0;
        foreach ($this->input->post('Nama') as $key => $item_id) {
            $number += 1;
            $data = array(
                'id_trans_komunitas_h'  => $code,
                'nama_det'              => $this->input->post('unama')[$key],
                'nik'                   => $this->input->post('unik')[$key]

            );
            $this->Noreg_model->insert_komunitasdetail($data);
        }
        $data = array(

            'id_komunitas'          => $this->input->post('id_komunitas'),
            'nik'                   => $this->input->post('nik'),
            'code'                  =>  $code,
            'nama'                  => $this->input->post('nama'),
            'info_bantuan'          => $this->input->post('info_bantuan'),
            'jumlah_bantuan'     => $this->input->post('jumlah_bantuan'),

            'id_program'            => 10,
            'periode_bantuan' => $this->input->post('periode_bantuan'),
            'total_periode' => $this->input->post('total_periode'),
            'rekomendasi_bantuan'   => $this->input->post('nama_pic'),
            'jumlah_pm'             => $this->input->post('jumlah_pm'),
            'asnaf'                 => $this->input->post('asnaf'),

            'id_subprogram'         => $this->input->post('id_subprogram'),
            'jenis_bantuan'         => $this->input->post('jenis_bantuan'),
            'sumber_dana'         => $this->input->post('sumber_dana'),
            'rencana_bantuan'       => $this->input->post('lokasi_program'),

            'deleted_at'            => '0000-00-00 00:00:00',
            'created_by'            => $this->session->username
        );

        $this->Noreg_model->insert_komunitas($data);
        write_log();
        $this->session->set_flashdata('message', '<div class="alert alert-success">Data saved succesfully</div>');
        redirect(base_url('noreg'));
    }
}
