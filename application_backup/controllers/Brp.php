<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Brp extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();

    $this->data['module'] = 'Brp';

    $this->load->model(array('Brp_model'));

    $this->load->helper(array('url', 'html'));
    $this->load->database();

    // company_profile
    $this->data['company_data']          = $this->Company_model->company_profile();

    // template
    $this->data['logo_header_template']  = $this->Template_model->logo_header();
    $this->data['navbar_template']       = $this->Template_model->navbar();
    $this->data['sidebar_template']      = $this->Template_model->sidebar();
    $this->data['background_template']   = $this->Template_model->background();
    $this->data['sidebarstyle_template'] = $this->Template_model->sidebarstyle();

    $this->data['btn_submit'] = '<button type="submit" name="button" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>';
    $this->data['btn_reset']  = 'Bersih';
    $this->data['btn_back']   = '<button type="button" onclick="history.back()" class="btn btn-secondary"> Kembali</button>';
    $this->data['btn_add']    = 'Add New Data';
    $this->data['add_action'] = base_url('lamustaindividu/create');
  }

  function konselor()
  {
    is_login();
    is_read();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title'] = $this->data['module'] . ' List';
    $this->data['header']     = $this->data['module'];
    $this->data['konselor']   = $this->Brp_model->get_all_individu();

    $data['path'] = base_url('assets');
    $this->load->view('back/brp/konselor/konselor_list', $this->data);
  }

  function create($id)
  {
    is_login();
    is_create();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title']       = 'Create New Manfaat BRP';
    $this->data['header']           =  $this->data['module'];
    $this->data['action_individu']  = 'brp/create_action_individu';
    $this->data['action_komunitas'] = 'brp/create_action_komunitas';

    $this->data['data']             = $this->Brp_model->get_data_individu($id);
    // $this->data['komunitas']        = $this->Brp_model->get_data_komunitas($id);
    $this->data['komunitas']        = $this->Brp_model->get_data_komunitas_by_program();
    $this->data['program']          = $this->Brp_model->get_program();
    $this->data['subprogram']       = $this->Brp_model->get_sub_program();
    $this->data['cek_individu']     = $this->Brp_model->check_transaksi_individu_by_id($id);
    $this->data['cek_komunitas']    = $this->Brp_model->check_transaksi_komunitas_by_id($id);
    $this->data['notif_individu']   = $this->Brp_model->get_transaksi_individu_by_date($id);
    $this->data['notif_komunitas']  = $this->Brp_model->get_transaksi_komunitas_by_date($id);
    $this->data['rekomender']       = $this->Brp_model->get_rekomender();
    $this->data['datapenduduk']       = $this->Brp_model->get_datapenduduk();
    $this->data['jk'] = [
      'name'          => 'jk',
      'id'            => 'jk',
      'class'         => 'form-control',
      'autocomplete'  => 'off',
      // 'required'      => '',
      'value'         => $this->form_validation->set_value('jk'),
    ];
    $this->data['jk_value'] = [
      ''              => 'Tidak ada keterangan',
      'L'             => 'Laki-laki',
      'P'             => 'Perempuan',
    ];

    // var_dump($this->Brp_model->get_datapenduduk());die;
    $this->load->view('back/brp/konselor/konselor_add', $this->data);
  }

  function create_action_individu()
  {
    $data = array(
      'id_individu' => $this->input->post('id_individu'),
      'nik' => $this->input->post('nik'),
      'nama' => $this->input->post('nama'),
      'id_program' => $this->input->post('id_program'),
      'id_subprogram' => $this->input->post('id_subprogram'),
      'info_bantuan'          => $this->input->post('info_bantuan'),

      'asnaf' => $this->input->post('asnaf'),

      'kasus' => "",
      'rekomendasi_bantuan' => "",
      'rencana_bantuan' => "",
      'mekanisme_bantuan' => "",

      'jumlah_bantuan'  => str_replace(",", "", $this->input->post('jumlah_bantuan')),
      'jenis_bantuan' => $this->input->post('jenis_bantuan'),
      'sifat_bantuan' => "",
      'bidang_tugas' => $this->input->post('bidang_tugas'),
      'profesi' => $this->input->post('profesi'),
      'kopentensi' => $this->input->post('kopentensi'),
      'periode_bantuan'         => $this->input->post('periode_bantuan'),
      'total_periode'         => $this->input->post('total_periode'),
      'sumber_dana'         => $this->input->post('sumber_dana'),

      'deleted_at'         => '0000-00-00 00:00:00',
      'created_by'         => $this->session->username
    );

    $this->Brp_model->insert($data);
    write_log();
    $this->session->set_flashdata('message', '<div class="alert alert-success">Data saved succesfully</div>');
    redirect(base_url('brp/konselor'));
  }

  function update_individu($id)
  {
    is_login();
    is_update();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title']  = 'Update Data ' . $this->data['module'];
    $this->data['update_action_individu'] = 'brp/update_action_individu';

    $this->data['data']        = $this->Brp_model->get_transaksi_individu_update($id);
    $this->data['rekomender']  = $this->Brp_model->get_rekomender();
    $this->data['subprogram']  = $this->Brp_model->get_sub_program();

    if ($this->data['data']) {
      $this->load->view('back/brp/konselor/konselor_edit_individu', $this->data);
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">Data not found</div>');
      redirect('brp/detail_konselor/' . $this->data['data'][0]['nik']);
    }
  }

  function update_action_individu()
  {
    $data = array(
      'id_individu' => $this->input->post('id_individu'),
      'nik' => $this->input->post('nik'),
      'nama' => $this->input->post('nama'),
      'id_program' => $this->input->post('id_program'),
      'id_subprogram' => $this->input->post('id_subprogram'),
      'info_bantuan'          => $this->input->post('info_bantuan'),

      'asnaf' => $this->input->post('asnaf'),

      'kasus' => "",
      'rekomendasi_bantuan' => "",
      'rencana_bantuan' => "",
      'mekanisme_bantuan' => "",

      'jumlah_bantuan'  => str_replace(",", "", $this->input->post('jumlah_bantuan')),
      'jenis_bantuan' => $this->input->post('jenis_bantuan'),
      'sifat_bantuan' => "",
      'bidang_tugas' => $this->input->post('bidang_tugas'),
      'profesi' => $this->input->post('profesi'),
      'kopentensi' => $this->input->post('kopentensi'),
      'periode_bantuan'         => $this->input->post('periode_bantuan'),
      'total_periode'         => $this->input->post('total_periode'),
      'sumber_dana'         => $this->input->post('sumber_dana'),

      'deleted_at'         => '0000-00-00 00:00:00',
      'created_by'         => $this->session->username
    );

    $where = $this->input->post('id_transaksi_individu');

    $this->Brp_model->update($where, $data);

    write_log();
    $this->session->set_flashdata('message', '
        <div class="row mt-3">
          <div class="col-md-12">
            <div class="alert alert-success">Data update succesfully</div>
          </div>
        </div>
      ');
    redirect('brp/detail_konselor/' . $this->input->post('nik'));
  }

  //----------------------- end lamusta individu --------------------------//


  function create_action_komunitas()
  {

    // var_dump($this->input->post('unik'));die;

    is_login();
    is_update();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title'] = 'Tambah Manfaat brp';
    $this->data['header']     = $this->data['module'];
    $jml = 0;
    $code  = $this->Brp_model->autonumber();
    foreach ($this->input->post('unama') as $key => $item_id) {
      $jml  += 1;
      $data = array(

        'id_trans_komunitas_h'  => $code,
        'nama_det'                   => $this->input->post('unama')[$key],
        'usia'     => $this->input->post('uusia')[$key],
        'jenis_kelamin' => $this->input->post('ujeniskelamin')[$key],
        'jenis_penyakit'         => $this->input->post('ujenispenyakit')[$key],
        'ruang_perawatan'          => $this->input->post('uruangperawatan')[$key],
        'kls_kamar'          => $this->input->post('ukelaskamar')[$key],
        'alamat'         => $this->input->post('ualamat')[$key],
        'kelurahan'          => $this->input->post('ukelurahan')[$key],
        'kecamatan'          => $this->input->post('ukecamatan')[$key],
        'kota'          => $this->input->post('ukota')[$key],
        'kesan'          => $this->input->post('ukesan')[$key],

      );
      $this->Brp_model->insert_komunitasdetail($data);
    }
    $data = array(

      'id_komunitas'          => $this->input->post('id_komunitas'),
      'nik'                   => $this->input->post('nik'),
      'code'                  =>  $code,
      'nama'                  => $this->input->post('nama'),
      'id_program'            => $this->input->post('id_program'),
      'id_subprogram'         => $this->input->post('id_subprogram'),
      'info_bantuan'          => $this->input->post('info_bantuan'),
      'bentuk_manfaat'        => "",
      'asnaf'                 => $this->input->post('asnaf'),
      'jumlah_pm'             => $jml,
      'rekomendasi_bantuan'   => "",
      'rencana_bantuan'       => "",
      'mekanisme_bantuan'     => "",
      'jumlah_bantuan'     => str_replace(",", "", $this->input->post('jumlah_bantuan')),
      'sumber_dana'         => $this->input->post('sumber_dana'),
      'jenis_bantuan'         => $this->input->post('jenis_bantuan'),
      'periode_bantuan'         => $this->input->post('periode_bantuan'),
      'total_periode'         => $this->input->post('total_periode'),
      'sifat_bantuan'         => "",
      'kunjungan'         => $this->input->post('kunjungan'),

      'deleted_at'            => '0000-00-00 00:00:00',
      'created_by'            => $this->session->username,
      'periode_bantuan'          => $this->input->post('tgl_kunjungan')
    );

    $this->Brp_model->insert_komunitas($data);



    write_log();
    $this->session->set_flashdata('message', '<div class="alert alert-success">Data saved succesfully</div>');
    redirect(base_url('brp/konselor'));
  }

  function update_komunitas($id)
  {
    is_login();
    is_update();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }
    // var_dump($this->Brp_model->get_transaksi_komunitasdetail_update($id));die;
    $this->data['page_title']  = 'Update Data ' . $this->data['module'];
    $this->data['update_action_komunitas'] = 'brp/update_action_komunitas';

    $this->data['data']          = $this->Brp_model->get_transaksi_komunitas_update($id);
    $this->data['detail']          = $this->Brp_model->get_transaksi_komunitasdetail_update($id);
    $this->data['komunitas']     = $this->Brp_model->get_data_komunitas();
    $this->data['program']       = $this->Brp_model->get_program();
    $this->data['rekomender']    = $this->Brp_model->get_rekomender();
    $this->data['subprogram']    = $this->Brp_model->get_sub_program();

    if ($this->data['data']) {
      $this->load->view('back/brp/konselor/konselor_edit_komunitas', $this->data);
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">Data not found</div>');
      redirect('brp/detail_konselor/' . $this->data['data'][0]['nik']);
    }
  }

  function update_action_komunitas()
  {
    is_login();
    is_update();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title'] = 'Edit Manfaat Lamusta';
    $this->data['header']     = $this->data['module'];
    $jml =  $this->input->post('jumlah_pm');
    $code =  $this->input->post('code');
    //  var_dump($code);die;
    foreach ($this->input->post('unama') as $key => $item_id) {
      $jml  += 1;
      $iddet =  $this->input->post('uiddet')[$key];
      $delete = $this->Brp_model->get_by_id_detail($iddet);
      if ($delete) {
        $this->Brp_model->deletedetail($iddet);
      }
      $data = array(

        'id_trans_komunitas_h'  => $code,
        'nama_det'                   => $this->input->post('unama')[$key],
        'usia'     => $this->input->post('uusia')[$key],
        'jenis_kelamin' => $this->input->post('ujeniskelamin')[$key],
        'jenis_penyakit'         => $this->input->post('ujenispenyakit')[$key],
        'ruang_perawatan'          => $this->input->post('uruangperawatan')[$key],
        'kls_kamar'          => $this->input->post('ukelaskamar')[$key],
        'alamat'         => $this->input->post('ualamat')[$key],
        'kelurahan'          => $this->input->post('ukelurahan')[$key],
        'kecamatan'          => $this->input->post('ukecamatan')[$key],
        'kota'          => $this->input->post('ukota')[$key],
        'kesan'          => $this->input->post('ukesan')[$key],

      );
      $this->Brp_model->insert_komunitasdetail($data);
    }
    $data = array(
      'id_komunitas'          => $this->input->post('id_komunitas'),
      'nik'                   => $this->input->post('nik'),
      'code'                  =>  $code,
      'nama'                  => $this->input->post('nama'),
      'id_program'            => $this->input->post('id_program'),
      'id_subprogram'         => $this->input->post('id_subprogram'),
      'info_bantuan'          => $this->input->post('info_bantuan'),
      'bentuk_manfaat'        => "",
      'asnaf'                 => $this->input->post('asnaf'),
      'jumlah_pm'             => $jml,
      'rekomendasi_bantuan'   => "",
      'rencana_bantuan'       => "",
      'mekanisme_bantuan'     => "",
      'jumlah_bantuan'     => str_replace(",", "", $this->input->post('jumlah_bantuan')),
      'sumber_dana'         => $this->input->post('sumber_dana'),
      'jenis_bantuan'         => $this->input->post('jenis_bantuan'),
      'periode_bantuan'         => $this->input->post('periode_bantuan'),
      'total_periode'         => $this->input->post('total_periode'),
      'sifat_bantuan'         => "",
      'kunjungan'         => $this->input->post('kunjungan'),

      'deleted_at'            => '0000-00-00 00:00:00',
      'created_by'            => $this->session->username,
      'periode_bantuan'          => $this->input->post('tgl_kunjungan')
    );

    $where = $this->input->post('id_transaksi_komunitas');

    $this->Brp_model->update_komunitas($where, $data);

    write_log();
    $this->session->set_flashdata('message', '
        <div class="row mt-3">
          <div class="col-md-12">
            <div class="alert alert-success">Data update succesfully</div>
          </div>
        </div>
      ');
    redirect('brp/detail_konselor/' . $this->input->post('nik'));
  }

  function detail_konselor($id)
  {
    is_login();
    is_read();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title']        = 'Detail ' . $this->data['module'];
    $this->data['header']            = $this->data['module'];
    $this->data['get_all_provinsi']  = $this->Brp_model->get_all_provinsi();
    $this->data['get_all_kabupaten'] = $this->Brp_model->get_all_kabupaten();
    $this->data['get_all_kecamatan'] = $this->Brp_model->get_all_kecamatan();
    $this->data['get_all_kelurahan'] = $this->Brp_model->get_all_kelurahan();
    $this->data['penduduk']          = $this->Brp_model->get_penduduk($id);


    $this->data['data_individu']     = $this->Brp_model->get_transaksi_individu_by_id($id);
    $this->data['data_komunitas']    = $this->Brp_model->get_transaksi_komunitas_by_id($id);
    $this->data['detail_individu']   = $this->Brp_model->get_detail_individu_by_id($id);
    $this->data['detail_komunitas']  = $this->Brp_model->get_detail_komunitas_by_id($id);

    $data['path'] = base_url('assets');
    $this->load->view('back/brp/konselor/konselor_detail', $this->data);
  }
  function delete_komunitas($id)
  {
    is_login();
    is_delete();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $delete = $this->Brp_model->delete_transaksi_komunitas_by_id($id);

    if ($delete) {
      $data = array(
        'is_delete'   => '1',
        'deleted_by'  => $this->session->username,
        'deleted_at'  => date('Y-m-d H:i:a'),
      );

      $this->Brp_model->soft_delete_komunitas($id, $data);

      $this->session->set_flashdata('message', '<div class="alert alert-success">Data deleted successfully</div>');
      redirect('brp/detail_konselor/' . $delete->nik);
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">No data found</div>');
      redirect('brp/detail_konselor');
    }
  }
  function laporan_pusat()
  {
    is_login();
    is_read();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title']        = 'Data ' . $this->data['module'];
    $this->data['header']            = 'Laporan Konselor';
    $this->data['get_all_provinsi']  = $this->Brp_model->get_all_provinsi();
    $this->data['get_all_kabupaten'] = $this->Brp_model->get_all_kabupaten();
    $this->data['get_all_kecamatan'] = $this->Brp_model->get_all_kecamatan();
    $this->data['get_all_kelurahan'] = $this->Brp_model->get_all_kelurahan();
    $this->data['individu']          = $this->Brp_model->get_all_admin_individu();
    $this->data['umur'] = [];
    $this->data['komunitas']         = $this->Brp_model->get_laporan_komunitas();
    for ($i = 0; $i < count($this->data['komunitas']); $i++) {
      $umur = $this->data['komunitas'][$i]->tgl_lahir;
      $res_umur = $this->hitung_umur($umur);
      array_push($this->data['umur'], $res_umur);
    }

    $this->data['komunitas_d'] = [];
    for ($i = 0; $i < count($this->data['komunitas']); $i++) {
      $id_transaksi_komunitas = $this->data['komunitas'][$i]->id_transaksi_komunitas;
      $sql = "SELECT
              `b`.`ruang_perawatan` as `ruang_perawatan`,
              `b`.`kls_kamar` as `kls_kamar`,
              `b`.`kesan` as `kesan`
              FROM `transaksi_komunitas` `a`
              JOIN `transaksi_komunitas_d` `b`
              ON `a`.`id_transaksi_komunitas` = `b`.`id_trans_komunitas_d`
              WHERE `a`.`id_transaksi_komunitas` = '$id_transaksi_komunitas'";
      $res = $this->db->query($sql)->row();
      array_push($this->data['komunitas_d'], $res);
    }
    // echo json_encode($this->data['komunitas_d']);
    // die;
    $data['path'] = base_url('assets');
    $this->load->view('back/brp/laporan/konselor', $this->data);
  }

  function hitung_umur($tanggal_lahir)
  {
    $birthDate = new DateTime($tanggal_lahir);
    $today = new DateTime("today");
    if ($birthDate > $today) {
      exit("0 tahun 0 bulan 0 hari");
    }
    $y = $today->diff($birthDate)->y;
    $m = $today->diff($birthDate)->m;
    $d = $today->diff($birthDate)->d;
    return $y . " tahun " . $m . " bulan " . $d . " hari";
  }

  //------------------------[ Lamusta Laporan Admin         ]-----------------------------//

  function laporan_admin()
  {
    is_login();
    is_read();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title']        = 'Data ' . $this->data['module'];
    $this->data['header']            = 'Laporan Admin';
    $this->data['get_all_provinsi']  = $this->Brp_model->get_all_provinsi();
    $this->data['get_all_kabupaten'] = $this->Brp_model->get_all_kabupaten();
    $this->data['get_all_kecamatan'] = $this->Brp_model->get_all_kecamatan();
    $this->data['get_all_kelurahan'] = $this->Brp_model->get_all_kelurahan();
    $this->data['individu']          = $this->Brp_model->get_all_admin_individu();
    $this->data['umur'] = [];
    for ($i = 0; $i < count($this->data['individu']); $i++) {
      $umur = $this->data['individu'][$i]->tgl_lahir;
      $res_umur = $this->hitung_umur($umur);
      array_push($this->data['umur'], $res_umur);
    }
    $this->data['komunitas']         = $this->Brp_model->get_laporan_komunitas();
    $list_umur_komunitas = [];
    for ($i = 0; $i < count($this->data['komunitas']); $i++) {
      $nik_user = $this->data['komunitas'][$i]->nik;
      $sql_umur_user = "SELECT
                        `a`.`tgl_lahir` as `tgl_lahir_user`
                        FROM `penduduk` `a`
                        JOIN `transaksi_komunitas` `b`
                        ON `a`.`nik` = `b`.`nik`
                        WHERE `b`.`nik` = '$nik_user'";
      $res_umur_user = $this->db->query($sql_umur_user)->row();
      array_push($list_umur_komunitas, $res_umur_user->tgl_lahir_user);
    }
    $this->data['umur_komunitas'] = [];
    for ($i = 0; $i < count($list_umur_komunitas); $i++) {
      $umur = $list_umur_komunitas[$i];
      $res_umur = $this->hitung_umur($umur);
      array_push($this->data['umur_komunitas'], $res_umur);
    }

    $data['path'] = base_url('assets');
    $this->load->view('back/brp/laporan/admin', $this->data);
  }
  function delete_individu($id)
  {
    is_login();
    is_delete();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $delete = $this->Brp_model->delete_transaksi_individu_by_id($id);

    if ($delete) {
      $data = array(
        'is_delete'   => '1',
        'deleted_by'  => $this->session->username,
        'deleted_at'  => date('Y-m-d H:i:a'),
      );

      $this->Brp_model->soft_delete($id, $data);

      $this->session->set_flashdata('message', '<div class="alert alert-success">Data deleted successfully</div>');
      redirect('brp/detail_konselor/' . $delete->nik);
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">No data found</div>');
      redirect('brp/detail_konselor' . $delete->nik);
    }
  }

  function delete_permanent_individu($id)
  {
    is_login();
    is_delete();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $delete = $this->Brp_model->get_by_id($id);

    if ($delete) {
      $this->Lamusta_model->delete($id);

      $this->session->set_flashdata('message', '<div class="alert alert-success">Data deleted permanently</div>');
      redirect('brp/deleted_list');
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">No data found</div>');
      redirect('brp/deleted_list');
    }
  }

  function delete_permanent_komunitas($id)
  {
    is_login();
    is_delete();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $delete = $this->Brp_model->delete_transaksi_komunitas_by_id($id);

    if ($delete) {
      $this->Brp_model->delete_permanent($id);

      $this->session->set_flashdata('message', '<div class="alert alert-success">Data deleted permanently</div>');
      redirect('brp/deleted_list');
    } else {
      $this->brp->set_flashdata('message', '<div class="alert alert-danger">No data found</div>');
      redirect('lamusta/deleted_list');
    }
  }
  function delete($id)
  {
    is_login();
    is_delete();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $delete = $this->Brp_model->get_by_id($id);

    if ($delete) {
      $data = array(
        'is_delete'   => '1',
        'deleted_by'  => $this->session->username,
        'deleted_at'  => date('Y-m-d H:i:a'),
      );

      $this->Brp_model->soft_delete($id, $data);

      // $this->write_log();

      $this->session->set_flashdata('message', '<div class="alert alert-success">Data deleted successfully</div>');
      redirect('lamustaindividu');
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">No data found</div>');
      redirect('lamustaindividu');
    }
  }


  function delete_permanent($id)
  {
    is_login();
    is_delete();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $delete = $this->Brp_model->get_by_id($id);

    if ($delete) {
      $this->Brp_model->delete($id);

      $this->session->set_flashdata('message', '<div class="alert alert-success">Data deleted permanently</div>');
      redirect('lamustaindividu/deleted_list');
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">No data found</div>');
      redirect('lamustaindividu/deleted_list');
    }
  }

  function deleted_list()
  {
    is_login();
    is_restore();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title'] = 'Deleted ' . $this->data['module'] . ' List';

    $this->data['komunitas'] = $this->Brp_model->get_all_deleted_komunitas();
    $this->data['individu']  = $this->Brp_model->get_all_deleted_individu();


    $this->load->view('back/brp/individu_deleted_list', $this->data);
  }

  function restore($id)
  {
    is_login();
    is_restore();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $row = $this->Brp_model->get_by_id($id);

    if ($row) {
      $data = array(
        'is_delete'   => '0',
        'deleted_by'  => NULL,
        'deleted_at'  => NULL,
      );

      $this->Brp_model->update($id, $data);

      $this->session->set_flashdata('message', '<div class="alert alert-success">Data restored successfully</div>');
      redirect('lamustaindividu/deleted_list');
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">No data found</div>');
      redirect('lamustaindividu/deleted_list');
    }
  }


  //------------------------[ DETAIL LAPORAN INDIVIDU & KOMUNITAS ]-----------------------------//
  function detail_laporan_individu($id)
  {
    is_login();
    is_update();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['get_all']      = $this->Brp_model->get_laporan_individu_detail($id);

    if ($this->data['get_all']) {
      $this->data['page_title'] = 'Detail Data ' . $this->data['module'];
      $this->load->view('back/brp/laporan/detail_laporan_individu', $this->data);
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">Penduduk not found</div>');
      redirect('brp');
    }
  }

  // -------------- DETAIL LAPORAN KOMNITAS --------------
  function detail_laporan_komunitas($id)
  {
    is_login();
    is_update();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['get_all']      = $this->Brp_model->get_laporan_komunitas_detail($id);

    if ($this->data['get_all']) {
      $this->data['page_title'] = 'Detail Data ' . $this->data['module'];
      $this->load->view('back/brp/laporan/detail_laporan_komunitas', $this->data);
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">Penduduk not found</div>');
      redirect('brp');
    }
  }
}
