<style>
    .bd-example-modal-lg .modal-dialog {
        display: table;
        position: relative;
        margin: 0 auto;
        top: calc(50% - 24px);
    }

    .bd-example-modal-lg .modal-dialog .modal-content {
        background-color: transparent;
        border: none;
    }
</style>
<!-- Begin Page Content -->
<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Marketing</h1>
    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">EDIT SPAK</h6>
        </div>
        <div class="card-body">
            <form action="<?php echo base_url('Marketing/UbahSpakAksi'); ?>" id="filter" method="post" enctype="multipart/form-data">
                <input type="text" class="form-control" hidden name="PesertaID" value="<?php echo $Premi[0]['PesertaID'] ?>">
                <input type="text" class="form-control" hidden name="PesertaProfileID" value="<?php echo $Premi[0]['PesertaProfileID'] ?>">

                <div class="row">
                    <div class="col-lg-6 d-flex">
                        <div class="card shadow mb-4 flex-fill">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Pemegang Polis</h6>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group"><label>Nama Pemegang Polis</label><input type="text" class="form-control" name="NamaPemegangPolis" value="<?php echo $Premi[0]['NamaPemegangPolis'] ?>"></div>
                                    </div>
                                </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Alamat Pemegang Polis</label>
                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="AlamatPemegangPolis"> <?php echo $Premi[0]['AlamatPemegangPolis'] ?></textarea>
                                    </div>
                                    <div class=" row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Nama PIC Pemegang Polis</label>
                                                <input type="text" class="form-control" name="NamaPIC" value="<?php echo $Premi[0]['NamaPIC'] ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Email PIC Pemegang Polis</label>
                                                <input type="email" class="form-control" name="EmailPIC" value="<?php echo $Premi[0]['EmailPIC'] ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Nomor Tlpn PIC Pemegang Polis</label>
                                                <input type="text" class="form-control" name="NoTlpnPIC" value="<?php echo $Premi[0]['NoTlpnPIC'] ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Nomor Polis</label>
                                                <input type="text" class="form-control" name="NomorPolis" value="<?php echo $Premi[0]['NomorPolis'] ?>"> 
                                            </div>
                                        </div>                                        
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>LOB</label>
                                                <input type="text" class="form-control" name="LOB" value="<?php echo $Premi[0]['LOB'] ?>">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Pembawa Bisnis</label>
                                                <select class="selectpicker form-control" data-live-search="true" id="ChannelDistribusiID" name="ChannelDistribusiID">
                                                    <option value="" selected disabled hidden>Pilih...</option>
                                                    <?php foreach ($ChannelDistribusi as $row) { ?>
                                                        <option value="<?php echo $row['ChannelDistribusiID']; ?>" <?php if ($row['ChannelDistribusiID'] == $Premi[0]['ChannelDistribusiID']) {echo "selected";}?>>
                                                            <?php echo $row['NamaPembawa'] ?>
                                                        </option>
                                                    <?php } ?> 
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                        
                        </div>
                        <div class="col-lg-6 d-flex">
                            <div class="card shadow mb-4 flex-fill">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Perusahaan Asuransi</h6>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label>Perusahaan Asuransi</label>
                                                        <input type="text" class="form-control" name="Perusahaan"  value="<?php echo $Premi[0]['Perusahaan']; ?>">
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Periode Asuransi Mulai</label>
                                                        <input type="date" class="form-control" name="PeriodeAwal" id="PeriodeAwal" value="<?php echo $Premi[0]['PeriodeAwal'] ?>"> 
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Periode Asuransi Akhir</label>
                                                        <input type="date" class="form-control" name="PeriodeAkhir" id="PeriodeAkhir" value="<?php echo $Premi[0]['PeriodeAkhir'] ?>">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="exampleFormControlInput1">TPA Fee</label>
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">Rp</span>
                                                            </div>
                                                            <input type="text" class="form-control" name="TpaFee" id="TpaFee" oninput="formatValue('TpaFee')" aria-describedby="basic-addon1" aria-label="TpaFee" value="<?php echo number_format($Premi[0]['TpaFee']); ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Brokerage Fee (BF) / Komisi</label>
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-BrokenFee">Rp</span>
                                                            </div>
                                                            <input type="text" class="form-control" name="BrokenFee" id="BrokenFee" oninput="formatValue('BrokenFee')" aria-describedby="basic-addon1" aria-label="BrokenFee" value="<?php echo number_format(intval($Premi[0]['BrokerageFee'])); ?>">
                                                        </div>

                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Metode Discount Apakah persen?</label>
                                                        <div class="custom-control custom-switch">
                                                            <input type="checkbox" class="custom-control-input" id="customSwitch1" name="DiscountAsuransi" <?php if ($Premi[0]['DiskonPremiType'] == 1) {echo "checked"; } ?>>
                                                            <label class="custom-control-label" for="customSwitch1">Persen</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label>Diskon Premi</label>
                                                        <?php if ($Premi[0]['DiskonPremiType'] == 1) { ?>
                                                            <div class="input-group mb-3" id="PersenAsuransi">
                                                                <input type="text" class="form-control" placeholder="" aria-label="Recipient's username" aria-describedby="basic-addon2" name="diskonpremipersenasuransi" value="<?php echo number_format($Premi[0]['DiskonPremi'], 0) ?>">
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text" id="basic-addon2">%</span>
                                                                </div>
                                                            </div>                                                            
                                                        <?php } else { ?>
                                                            <div class="input-group mb-3" id="NumberAsuransi">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" id="basic-addon1">Rp</span>
                                                                </div>
                                                                <input type="text" class="form-control" name="diskonpremiasuransi" placeholder="" aria-label="Username" aria-describedby="basic-addon1" value="<?php echo number_format($Premi[0]['DiskonPremi'], 0) ?>">
                                                            </div>                                                            
                                                        <?php } ?>                                                        
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="customRange3">Banyaknya pembayaran</label>
                                                        <input type="number" class="form-control" min="0" max="12" value="<?php echo count($fpi); ?>" id="myRange" name="metodepembayaran">
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="pembayaranpremi">
                                                <?php
                                                $i = 1;
                                                foreach ($fpi as $row) { ?>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label>Tanggal Premi ke - <?= $i ?></label>
                                                                <input type="date" value="<?php echo $row["TanggalPremi"] ?>" name="tanggalpremi<?= $i ?>" class="form-control" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label>Pesentase Pembayaran ke - <?= $i ?></label>
                                                                <div class='input-group mb-3'>
                                                                    <input type="number" value="<?php echo $row["Persentasi"] ?>" name="pesentase<?= $i ?>" class="form-control" aria-describedby='Persentasi' readonly>
                                                                    <div class='input-group-append'>
                                                                        <span class='input-group-text' id='Persentasi'>%</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group">
                                                                <label>Nominal Pembayaran ke - <?= $i ?></label>
                                                                <div class='input-group mb-3'>
                                                                    <div class='input-group-append'>
                                                                        <span class='input-group-text' id='nominal2'>Rp</span>
                                                                    </div>
                                                                    <input type="number" name="nominal<?= $i ?>" value="<?php echo $row['Nominal'] ?>" class="form-control" aria-describedby='nominal2' readonly>
                                                                </div>
                                                            </div>
                                                        </div>                            
                                                    </div>
                                                <?php
                                                    $i = $i + 1;
                                                } ?>
                                            </div>
                                            
                                            <input type="hidden" class="form-control" placeholder="" aria-label="Recipients username" aria-describedby="basic-addon2" name="diskonpremipersenpemegang"  value="<?php echo number_format($Premi[0]['DiskonPremiPemegang'], 0) ?>">
                                            <input type="hidden" class="form-control" name="diskonpremipemegang" placeholder="" aria-label="Username" aria-describedby="basic-addon1"  value="<?php echo number_format($Premi[0]['DiskonPremiPemegang'], 0) ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Umur Anak Peserta</h6>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Umur Anak General</label>
                                                <input type="text" class="form-control" value="<?php echo $Premi[0]['AnakGol1'] ?>" name="AnakGol1">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Sampai dengan</label>
                                                <input type="text" class="form-control" value="<?php echo $Premi[0]['AnakGol1sd'] ?>" name="AnakGol1sd" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Umur Anak Female</label>
                                                <input type="text" class="form-control" value="<?php echo $Premi[0]['AnakGol1F'] ?>" name="AnakGol1F">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Sampai dengan</label>
                                                <input type="text" class="form-control" value="<?php echo $Premi[0]['AnakGol1Fsd'] ?>" name="AnakGol1Fsd">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Umur Anak Male</label>
                                                <input type="text" class="form-control" value="<?php echo $Premi[0]['AnakGol1m'] ?>" name="AnakGolm1">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Sampai dengan</label>
                                                <input type="text" class="form-control" value="<?php echo $Premi[0]['AnakGol1msd'] ?>" name="AnakGol1msd">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 d-flex">
                            <div class="card shadow mb-4 flex-fill">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">List Dokumen</h6>
                                </div>
                                <div class="card-body">
                                    <ol>
                                        <?php foreach ($Dokumen as $row) { ?>
                                            <li style="margin-left: -25px;">
                                                <div class="form-group"> 
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" value="<?php echo $row['PesertaFileName'] ?>" disabled>
                                                        <div class="input-group-append">
                                                            <?php $path_parts = pathinfo(base_url() . $row['Url']);
                                                            $fileExtension = $path_parts['extension'];
                                                            if ($fileExtension == 'xls' || $fileExtension == 'xlsx' || $fileExtension == 'doc' || $fileExtension == 'xlsx' || $fileExtension == 'pdf' || $fileExtension == 'ppt' || $fileExtension == 'pptx') { ?>
                                                                <a class="btn btn-primary" href="https://docs.google.com/viewer?url=<?php echo base_url() . $row['Url'] ?>" target="_blank" title="Lihat <?php echo strtolower($row['PesertaFileName']) ?>?">
                                                                    <i class=" fa fa-eye"></i>
                                                                </a>
                                                            <?php }else{ ?>
                                                                <a class="btn btn-primary" href="<?php echo base_url() . $row['Url'] ?>" target="_blank" title="Lihat <?php echo strtolower($row['PesertaFileName']) ?>?">
                                                                    <i class=" fa fa-eye"></i>
                                                                </a>
                                                            <?php } ?>
                                                            <a class="btn btn-success" href="<?php echo base_url() . $row['Url'] ?>" target="_blank" title="Download <?php echo strtolower($row['PesertaFileName']) ?>?" download>
                                                                <i class="fa fa-download"></i>
                                                            </a> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>                                                    
                                        <?php } ?>
                                    </ol>
                                </div>
                            </div>
                        </div>  
                        <div class="col-lg-12">
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <div class="row">
                                        <div class="col-lg-6 col-sm-6">
                                            <h6 class="m-0 font-weight-bold text-primary">Tambah Dokumen</h6>
                                        </div>
                                        <div class="col-lg-6 col-sm-6 text-right">
                                            <a class="btn btn-danger btn-sm" onclick="kurangiDokumen()">
                                                <i class="fa fa-times-circle"></i> Delete
                                            </a>
                                            <a class="btn btn-primary btn-sm" onclick="createNewElement()">
                                                &nbsp; <i class="fa fa-plus-circle"></i> Add &nbsp;
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row" id="inputandokumen">
                                        <input type="text" hidden name="banyakdokumenpendukung" id="banyakdokumenpendukung" value=1>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>1. Document</label>
                                                <input type="file" name="Dokumenpendukung1" class="form-control" id="" aria-describedby="inputGroupFileAddon01">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                    <div class="row mt-3">
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Simpan</button>
                        </div>
                        <div class="col-lg-6">
                            <button type="button" class="btn btn-secondary btn-lg btn-block" onclick="history.back()">Batal</button>
                        </div>
                    </div>
                </form>
            </div>                
        </div> 
</div>

<div class="modal fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="width: 48px">
            <span class="fa fa-spinner fa-spin fa-3x"></span>
        </div>
    </div>
</div>

<!-- Modal Edit -->
<script type="text/javascript">
    function modal() {
        $('.modal').modal('show');
    }
    $(document).ready(function() {
        $('#table_id').DataTable({
            "scrollX"   : true,
            "pageLength": 25,
        });
        var pemegangbaru   = 0
        var persenasuransi = 0
        var persenpemegang = 0
        $('#PersenAsuransi').hide()
        $('#PersenPemegang').hide()
        $('#PemegangBaru').hide()
        $('#customSwitch').on('change', function() {
            if (pemegangbaru == 0) {
                $('#PemegangBaru').show()
                $('#PemegangLama').hide()
                pemegangbaru = 1
            } else {
                $('#PemegangBaru').hide()
                $('#PemegangLama').show()
                pemegangbaru = 0
            }
        })
        
        $('#customSwitch1').on('change', function() {
            if (persenasuransi == 0) {
                $('#PersenAsuransi').show()
                $('#NumberAsuransi').hide()
                persenasuransi = 1
            } else {
                $('#PersenAsuransi').hide()
                $('#NumberAsuransi').show()
                persenasuransi = 0
            }
        })
        $('#customSwitch2').on('change', function() {
            if (persenpemegang == 0) {
                $('#PersenPemegang').show()
                $('#NumberPemegang').hide()
                persenpemegang = 1
            } else {
                $('#NumberPemegang').show()
                $('#PersenPemegang').hide()
                persenpemegang = 0
            }
        })
    });

    var banyaknyainputan = 1;
    function createNewElement() {
        var txtNewInputBox = document.createElement('div');
        txtNewInputBox.className = "col-sm-12";
        banyaknyainputan += 1;
        $('#banyakdokumenpendukung').val(banyaknyainputan);
        txtNewInputBox.setAttribute("id", "DokumenPendukungForm" + banyaknyainputan);
        txtNewInputBox.innerHTML = "<div class='form-group'><label>" + banyaknyainputan + ". Dokumen</label><input type='file' name='Dokumenpendukung" + banyaknyainputan + "' class='form-control' id='inputGroupFile01' aria-describedby='inputGroupFileAddon01'></div>";
        document.getElementById("inputandokumen").appendChild(txtNewInputBox);
    }
    function kurangiDokumen() {
        var myobj = document.getElementById("DokumenPendukungForm" + banyaknyainputan);
        myobj.remove();
        banyaknyainputan = banyaknyainputan - 1;
        $('#banyakdokumenpendukung').val(banyaknyainputan);
    }

    function formatValue(id) {
        var x = document.getElementById(id).value;
        if (/^[0-9.,]+$/.test(x)) {
            document.getElementById(id).value = parseFloat(
                x.replace(/,/g, "")
            ).toLocaleString("en");
        } else {
            document.getElementById(id).value = x.substring(0, x.length - 1);
        }
    }

    $("a[data-toggle=\"tab\"]").on("shown.bs.tab", function(e) {
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });
    <?php if ($warning == 1) { ?>
        Swal.fire(
            'Gagal',
            'Silahkan Isi semua Form!',
            'error'
        )
    <?php } ?>
    <?php if ($warning == 3) { ?>
        Swal.fire(
            'Berhasil',
            'Berhasil Terdaftar',
            'success'
        )
    <?php } ?>

    $('#myRange, #PeriodeAwal').on('keyup', function() {
        var i = 1;
        jQuery('#pembayaranpremi').html('');
        date = $('#PeriodeAwal').val()
        var newDate = new Date(date);
        penambahan = 12 / $('#myRange').val();
        var pesentasi = 100 / $('#myRange').val();
        var nominal = "<?php echo $Premi[0]['BrokerageFee'] ?>" / $('#myRange').val();
        while (i <= $('#myRange').val()) {
            var newDates = newDate.toISOString().slice(0, 10);
            var txtNewInputBox = document.createElement('div');
            txtNewInputBox.innerHTML = "<div class='row'><div class='col-lg-4'><div class='form-group'><label>Tanggal Premi ke - " + i + "</label><input type='date' value='" + newDates + "' name='tanggalpremi" + i + "' class='form-control' ></div></div> <div class='col-lg-4'><div class='form-group'><label>Pesentase Pembayaran ke - " + i + "</label><div class='input-group mb-3'><input type='text' value='" + pesentasi + "' name='pesentase" + i + "' class='form-control' aria-describedby='basic-addon2' readonly required><div class='input-group-append'><span class='input-group-text' id='basic-addon2'>%</span></div></div></div></div><div class='col-lg-4'><div class='form-group'><label>Nominal Pembayaran ke - " + i + "</label><div class='input-group mb-3'><div class='input-group-append'><span class='input-group-text' id='nominal2'>Rp</span></div><input type='number' value='" + Math.ceil(nominal) + "' name='nominal" + i + "' class='form-control' aria-describedby='nominal2' readonly required></div></div></div></div>";
            document.getElementById("pembayaranpremi").appendChild(txtNewInputBox);
            i++;
            newDate = new Date(newDate.setMonth(newDate.getMonth() + penambahan));
        }
    });

    $('#myRange, #PeriodeAwal').on('change', function() {
        var i = 1;
        jQuery('#pembayaranpremi').html('');
        date = $('#PeriodeAwal').val()
        var newDate = new Date(date);
        penambahan = 12 / $('#myRange').val();
        var pesentasi = 100 / $('#myRange').val();
        var nominal = "<?php echo $Premi[0]['BrokerageFee'] ?>" / $('#myRange').val();
        while (i <= $('#myRange').val()) {
            var newDates = newDate.toISOString().slice(0, 10);
            var txtNewInputBox = document.createElement('div');
            txtNewInputBox.innerHTML = "<div class='row'><div class='col-lg-4'><div class='form-group'><label>Tanggal Premi ke - " + i + "</label><input type='date' value='" + newDates + "' name='tanggalpremi" + i + "' class='form-control' ></div></div> <div class='col-lg-4'><div class='form-group'><label>Pesentase Pembayaran ke - " + i + "</label><div class='input-group mb-3'><input type='text' value='" + pesentasi + "' name='pesentase" + i + "' class='form-control' aria-describedby='basic-addon2' readonly required><div class='input-group-append'><span class='input-group-text' id='basic-addon2'>%</span></div></div></div></div><div class='col-lg-4'><div class='form-group'><label>Nominal Pembayaran ke - " + i + "</label><div class='input-group mb-3'><div class='input-group-append'><span class='input-group-text' id='nominal2'>Rp</span></div><input type='number' value='" + Math.ceil(nominal) + "' name='nominal" + i + "' class='form-control' aria-describedby='nominal2' readonly required></div></div></div></div>";
            document.getElementById("pembayaranpremi").appendChild(txtNewInputBox);
            i++;
            newDate = new Date(newDate.setMonth(newDate.getMonth() + penambahan));
        }
    });

    $('#inputGroupFile01').on('change', function() {
        var fileName = $(this).val();
        console.log(fileName);
        $(this).next('.labelpremi').html(fileName);
    })
    $('#inputGroupFile02').on('change', function() {
        var fileName = $(this).val();
        $(this).next('.filepeserta').html(fileName);
    })
</script>