<?php $this->load->view('back/template/meta'); ?>
<?php $this->load->view('back/template/header'); ?>
<?php $this->load->view('back/template/sidebar'); ?>

<div class="main-panel">
    <div class="main-content">
      <div class="content-wrapper">
        <section class="basic-elements">
          <div class="row">
            <div class="col-sm-12">
              <div class="content-header">Data PKM</div>
            </div>
          </div>
		  		<?php if($this->session->flashdata('message')){echo $this->session->flashdata('message');} ?>
          <div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-body">  
									<div class="row">
									<table id="datatable" class="table table-striped nowrap text-capitalize" width="100%">
										<thead>
											<tr>
								        <th class="text-center">No</th>
								        <th class="text-center">No KTP</th>
												<th class="text-center">No KK</th>
								        <th class="text-center">Nama</th>					                
								        <th class="text-center">Lokasi</th>
								        <th class="text-center">Action</th>
								      </tr>
					          </thead>
					        	<tbody>
											<?php $no = 1; foreach($konselor as $data) { ?>
								      <tr>
								        <td class="text-center"><?php echo $no ?></td>
								        <td><?php echo $data->nik ?></td>
												<td><?php echo $data->no_kk ?></td>
								        <td><?php echo $data->nama ?></td>
												<td><?php echo $data->desa_kelurahan ?></td>
								        <td class="text-center">
								          <a href="<?php echo base_url('pkm/detail_konselor/'.$data->nik) ?>" class="btn btn-primary btn-sm" title="Detail"><i class="fa fa-eye"></i></a>
								          <a href="<?php echo base_url('pkm/create/').$data->nik ?>" class="btn btn-sm btn-success" title="Tambah Manfaat"><i class="fa fa-plus-circle"></i></a>
								        </td>
								      </tr>
											<?php 
												$no += 1;
											} ?>
					          </tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
	    </section>
          <br><br><br><br>
        </div>
    </div> 
</div>

<?php $this->load->view('back/template/footer'); ?>