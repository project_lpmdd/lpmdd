<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barzah extends CI_Controller{

  public function __construct()
  {
    parent::__construct();

    $this->data['module'] = 'Barzah';

    $this->load->model(array('Barzah_model'));

    $this->load->helper(array('url', 'html'));
    $this->load->database();

    // company_profile
    $this->data['company_data']               = $this->Company_model->company_profile();

    // template
    $this->data['logo_header_template']       = $this->Template_model->logo_header();
    $this->data['navbar_template']            = $this->Template_model->navbar();
    $this->data['sidebar_template']           = $this->Template_model->sidebar();
    $this->data['background_template']        = $this->Template_model->background();
    $this->data['sidebarstyle_template']      = $this->Template_model->sidebarstyle();

    $this->data['btn_submit'] = '<button type="submit" name="button" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>';
    $this->data['btn_reset']  = 'Bersih';
    $this->data['btn_back']   = '<button type="button" onclick="history.back()" class="btn btn-success"> Kembali</button>';
    $this->data['btn_add']    = 'Add New Data';
    $this->data['add_action'] = base_url('barzah/create');
  }

  function index()
  {
    is_login();
    is_read();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title'] = $this->data['module'] . ' List';
    $this->data['header'] = $this->data['module'];

    $this->data['get_all'] = $this->Barzah_model->get_all_barzah();


    $data['path'] = base_url('assets');

    $this->load->view('back/barzah/barzah_list', $this->data);
  }

  public function get_data_penduduk()
  {
    $nik  = $this->input->get('nik');
    $data = $this->Barzah_model->get_nik_penduduk($nik);
    echo json_encode($data);
  }

  public function get_data_barzah()
  {
    $nama_barzah  = $this->input->get('nama_barzah');
    $data = $this->Barzah_model->get_data_barzah($nama_barzah);
    echo json_encode($data);
  }


  function create()
  {
    is_login();
    is_create();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title'] = 'Create New ' . $this->data['module'];
    $this->data['sub']        = "Tambah barzah";
    $this->data['header']     = $this->data['module'];
    $this->data['action_individu']  = "barzah/create_action_individu";
    $this->data['action_komunitas'] = 'barzah/create_action_komunitas';

    // $this->data['get_all_combobox_usertype']     = $this->Usertype_model->get_all_combobox();
    // $this->data['get_all_combobox_data_access']  = $this->Dataaccess_model->get_all_combobox();
    $this->data['komunitas']        = $this->Barzah_model->get_data_komunitas();
    $this->data['program']          = $this->Barzah_model->get_program();
    $this->data['subprogram']       = $this->Barzah_model->get_sub_program();
    $this->data['get_penduduk']     = $this->Barzah_model->get_penduduk();
    $this->data['get_all_provinsi'] = $this->Barzah_model->get_all_provinsi();
    $this->data['get_negara']       = $this->Barzah_model->get_negara();
    $this->data['rekomender']       = $this->Barzah_model->get_rekomender();
    $this->data['petugas']          = $this->Barzah_model->get_petugas();
    $this->data['individu']         = $this->Barzah_model->get_individu(); 
  
    $this->load->view('back/barzah/barzah_add', $this->data);
  }

  function create_action_individu()
  { 
    $id  = $this->input->post('nik');
    $row = $this->Barzah_model->get_check($id);
    if ($row->nik && $row->is_delete == 0) {
      write_log();
      $this->session->set_flashdata('message', '<div class="alert bg-danger alert-dismissible mb-2" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                  No KTP already exists.
                </div>');
      redirect('barzah');
    }elseif ($row->nik && $row->is_delete == 1) {
      write_log();
      $this->session->set_flashdata('message', '<div class="alert bg-danger alert-dismissible mb-2" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                   No KTP already exists in Recycle Bin.
                </div>');
      redirect('barzah');
    }else{
      $this->form_validation->set_rules('nik', 'nik', 'required');
      $this->form_validation->set_rules('nama', 'nama', 'trim|required');
      $this->form_validation->set_rules('sebab_kematian', 'sebab_kematian', 'required');
      $this->form_validation->set_rules('bentuk_layanan', 'bentuk_layanan', 'required');
      $this->form_validation->set_rules('tempat_meninggal', 'tempat_meninggal', 'required');
      $this->form_validation->set_rules('meninggalkan', 'meninggalkan', 'required');      
      $this->form_validation->set_rules('info_bantuan', 'info_bantuan', 'required');
      $this->form_validation->set_rules('petugas', 'petugas', 'required');

      $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

      if($this->form_validation->run() === FALSE)
      {
        $this->create();
      }
      else
      {
        //insert ke table barzah
        $data = array(
          'nik'                => $this->input->post('nik'),
          'nama'               => $this->input->post('nama'),
          'sebab_kematian'     => $this->input->post('sebab_kematian'),
          'bentuk_layanan'     => $this->input->post('bentuk_layanan'),
          'tempat_meninggal'   => $this->input->post('tempat_meninggal'),
          'meninggalkan'       => $this->input->post('meninggalkan'),
          'deleted_at'         => '0000-00-00 00:00:00',
          'created_by'         => $this->session->username,
        );
        $this->Barzah_model->insert($data);
        write_log();
        //insert ke table transaksi_individu
        $data = array(
          'id_individu'        => $this->input->post('id_individu'),
          'nik'                => $this->input->post('nik'),
          'nama'               => $this->input->post('nama'),
          'sebab_kematian'     => $this->input->post('sebab_kematian'),
          'jenis_bantuan'      => $this->input->post('bentuk_layanan'),
          'tempat_meninggal'   => $this->input->post('tempat_meninggal'),
          'meninggalkan'       => $this->input->post('meninggalkan'),
          'info_bantuan'       => $this->input->post('info_bantuan'),
          'petugas'            => $this->input->post('petugas'),          
          'periode_bantuan'    => date('Y-m-d'),
          'total_periode'      => '1',
          'id_program'         => '9',
          'id_subprogram'      => $this->input->post('id_subprogram'),
          'deleted_at'         => '0000-00-00 00:00:00',
          'created_by'         => $this->session->username,
        );

        $this->Barzah_model->insert_individu($data);
        write_log();

        $this->session->set_flashdata('message', '<div class="alert alert-success">Data saved succesfully</div>');
        redirect('barzah');

      }
    }
  }

  function update($id)
  {
    is_login();
    is_update();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $nik = $this->input->post('nik');

    $this->data['page_title'] = 'Update Data ' . $this->data['module'];
    $this->data['action']     = 'barzah/update_action';

    $this->data['get_penduduk']     = $this->Barzah_model->get_penduduk();
    $this->data['get_all_provinsi'] = $this->Barzah_model->get_all_provinsi();
    $this->data['get_negara']       = $this->Barzah_model->get_negara();
    $this->data['barzah']           = $this->Barzah_model->get_by_nik($id);

    if ($this->data['barzah']) {

      $this->data['id_barzah'] = [
        'name'          => 'id_barzah',
        'type'          => 'hidden',
      ];

      $this->data['nik'] = [
        'name'          => 'nik',
        'id'            => 'nik',
        'class'         => 'form-control',
        'autocomplete'  => 'off',
        'required'      => '',
      ];
      $this->data['nama'] = [
        'name'          => 'nama',
        'id'            => 'nama',
        'class'         => 'form-control text-capitalize',
        'autocomplete'  => 'off',
        'required'      => '',
        'readonly'      => 'readonly',
      ];
      $this->data['sebab_kematian'] = [
        'name'          => 'sebab_kematian',
        'id'            => 'sebab_kematian',
        'class'         => 'form-control',
        'autocomplete'  => 'off',
        'required'      => '',
      ];
      $this->data['tempat_meninggal'] = [
        'name'          => 'tempat_meninggal',
        'id'            => 'tempat_meninggal',
        'class'         => 'form-control',
        'autocomplete'  => 'off',
        'rows'          => '3',
      ];
      $this->data['meninggalkan'] = [
        'name'          => 'meninggalkan',
        'id'            => 'meninggalkan',
        'class'         => 'form-control',
        'autocomplete'  => 'off',
        'rows'          => '3',
      ];
      $this->data['bentuk_layanan'] = [
        'name'          => 'bentuk_layanan',
        'id'            => 'bentuk_layanan',
        'class'         => 'form-control',
        'autocomplete'  => 'off',
        'required'      => '',
      ];

      $this->load->view('back/barzah/barzah_edit', $this->data);
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">Penduduk not found</div>');
      redirect('barzah/detail/' . $nik);
    }
  }

  function update_action()
  {

    $this->form_validation->set_rules('nik', 'nik', 'required');
    $this->form_validation->set_rules('nama', 'nama', 'trim|required');
    $this->form_validation->set_rules('sebab_kematian', 'sebab_kematian', 'required');
    $this->form_validation->set_rules('bentuk_layanan', 'bentuk_layanan', 'required');
    $this->form_validation->set_rules('tempat_meninggal', 'tempat_meninggal', 'required');
    $this->form_validation->set_rules('meninggalkan', 'meninggalkan', 'required');

    $nik = $this->input->post('nik');

    $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

    if ($this->form_validation->run() === FALSE) {
      $this->update($this->input->post('id_barzah'));
    } else {
      $data = array(
        'nik'                => $this->input->post('nik'),
        'nama'               => $this->input->post('nama'),
        'sebab_kematian'     => $this->input->post('sebab_kematian'),
        'bentuk_layanan'     => $this->input->post('bentuk_layanan'),
        'tempat_meninggal'   => $this->input->post('tempat_meninggal'),
        'meninggalkan'       => $this->input->post('meninggalkan'),
        'deleted_at'         => '0000-00-00 00:00:00',
        'created_by'         => $this->session->username,
      );

      $this->Barzah_model->update($this->input->post('id_barzah'), $data);

      write_log();
      $this->session->set_flashdata('message', '<div class="alert alert-success">Data update succesfully</div>');
      redirect('barzah/detail/' . $nik);
    }
  }

  function create_action_komunitas()
  {
    $this->form_validation->set_rules('id_komunitas', 'id_komunitas', 'required');
    $this->form_validation->set_rules('id_program', 'id_program', 'required');
    $this->form_validation->set_rules('id_subprogram', 'id_subprogram', 'required');
    $this->form_validation->set_rules('bentuk_layanan', 'bentuk_layanan', 'required');
    $this->form_validation->set_rules('jml_individu', 'jml_individu', 'required');
    $this->form_validation->set_rules('alamat', 'alamat', 'required');
    $this->form_validation->set_rules('id_provinsi', 'id_provinsi', 'required');
    $this->form_validation->set_rules('id_kota_kab', 'id_kota_kab', 'required');
    $this->form_validation->set_rules('id_kecamatan', 'id_kecamatan', 'required');
    $this->form_validation->set_rules('id_desa_kelurahan', 'id_desa_kelurahan', 'required');
    $this->form_validation->set_rules('asnaf', 'asnaf', 'required');
    $this->form_validation->set_rules('sumber_dana', 'sumber_dana', 'required');
    $this->form_validation->set_rules('periode_bantuan', 'periode_bantuan', 'required');
    $this->form_validation->set_rules('total_periode', 'total_periode', 'required');
    $this->form_validation->set_rules('jumlah_bantuan', 'jumlah_bantuan', 'required');

    $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

    if ($this->form_validation->run() === FALSE) {
      $this->create();
    } else {
      $code  = $this->Barzah_model->autonumber();
      $data = array(
        'code'                         =>  $code,
        'id_komunitas'                 => $this->input->post('id_komunitas'),
        'id_program'                   => $this->input->post('id_program'),
        'id_subprogram'                => $this->input->post('id_subprogram'),
        'bentuk_manfaat'               => $this->input->post('bentuk_layanan'),
        'jumlah_pm'                    => $this->input->post('jml_individu'),
        'alamat'                       => $this->input->post('alamat'),
        // 'provinsi'                       => $this->input->post('id_provinsi'),
        // 'kota'                       => $this->input->post('id_kota_kab'),
        // 'kecamatan'                       => $this->input->post('id_kecamatan'),
        // 'kelurahan'                       => $this->input->post('id_desa_kelurahan'),
        'asnaf'                        => $this->input->post('asnaf'),
        'sumber_dana'                  => $this->input->post('sumber_dana'),
        'periode_bantuan'              => $this->input->post('periode_bantuan'),
        'total_periode'                => $this->input->post('total_periode'),
        'jumlah_bantuan'                => str_replace(",", "", $this->input->post('jumlah_bantuan')),
        'deleted_at'         => '0000-00-00 00:00:00',
        'created_by'         => $this->session->username,
      );
      $this->Barzah_model->insert_komunitas($data);

      $data = array(
        'id_trans_komunitas_h'         =>  $code,
        'alamat'                       => $this->input->post('alamat'),
        'kelurahan'                    => $this->input->post('id_desa_kelurahan'),
        'kecamatan'                    => $this->input->post('id_kecamatan'),
        'kota'                         => $this->input->post('id_kota_kab'),
      );
      $this->Barzah_model->insert_komunitasdetail($data);

      write_log();

      $this->session->set_flashdata('message', '<div class="alert alert-success">Data saved succesfully</div>');
      redirect('barzah/laporan_pusat');
    }
  }


  function view($id)
  {
    is_login();
    is_update();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['get_all_provinsi']  = $this->Barzah_model->get_all_provinsi();
    $this->data['get_all_kabupaten'] = $this->Barzah_model->get_all_kabupaten();
    $this->data['get_all_kecamatan'] = $this->Barzah_model->get_all_kecamatan();
    $this->data['get_all_kelurahan'] = $this->Barzah_model->get_all_kelurahan();
    $this->data['barzah']            = $this->Barzah_model->get_nik_penduduk($id);

    if ($this->data['barzah']) {
      $this->data['page_title'] = 'Detail Data';

      $this->load->view('back/barzah/barzah_view', $this->data);
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">Penduduk not found</div>');
      redirect('barzah/detail');
    }
  }

  function detail($id)
  {
    is_login();
    is_update();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['get_all']      = $this->Barzah_model->get_detail_by_nama_barzah($id);

    if ($this->data['get_all']) {
      $this->data['page_title'] = 'Detail Data ' . $this->data['module'];
      $this->data['action']     = 'barzah/update_action';

      $this->load->view('back/barzah/barzah_list_detail', $this->data);
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">Penduduk not found</div>');
      redirect('barzah');
    }
  }

  function delete($id)
  {
    is_login();
    is_delete();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $delete = $this->Barzah_model->get_by_id($id);

    if ($delete) {
      $data = array(
        'is_delete'   => '1',
        'deleted_by'  => $this->session->username,
        'deleted_at'  => date('Y-m-d H:i:a'),
      );

      $this->Barzah_model->soft_delete($id, $data);

      // $this->write_log();

      $this->session->set_flashdata('message', '<div class="alert alert-success">Data deleted successfully</div>');
      redirect('barzah');
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">No data found</div>');
      redirect('barzah');
    }
  }


  function delete_permanent($id)
  {
    is_login();
    is_delete();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $delete = $this->Barzah_model->get_by_id($id);

    if ($delete) {
      $dir        = "./assets/images/user/" . $delete->photo;
      $dir_thumb  = "./assets/images/user/" . $delete->photo_thumb;

      if (is_file($dir)) {
        unlink($dir);
        unlink($dir_thumb);
      }

      $this->Barzah_model->delete($id);

      $this->session->set_flashdata('message', '<div class="alert alert-success">Data deleted permanently</div>');
      redirect('barzah/deleted_list');
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">No data found</div>');
      redirect('barzah/deleted_list');
    }
  }

  function deleted_list()
  {
    is_login();
    is_restore();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title'] = 'Deleted ' . $this->data['module'] . ' List';

    $this->data['get_all_deleted'] = $this->Barzah_model->get_all_deleted();


    $this->load->view('back/barzah/barzah_deleted_list', $this->data);
  }

  function restore($id)
  {
    is_login();
    is_restore();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $row = $this->Barzah_model->get_by_id($id);

    if ($row) {
      $data = array(
        'is_delete'   => '0',
        'deleted_by'  => NULL,
        'deleted_at'  => NULL,
      );

      $this->Barzah_model->update($id, $data);

      $this->session->set_flashdata('message', '<div class="alert alert-success">Data restored successfully</div>');
      redirect('barzah/deleted_list');
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">No data found</div>');
      redirect('barzah/deleted_list');
    }
  }

  function laporan_pusat()
  {
    is_login();
    is_read();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title']        = 'Data ' . $this->data['module'];
    $this->data['header']            = 'Laporan Pusat';
    $this->data['get_all_provinsi']  = $this->Barzah_model->get_all_provinsi();
    $this->data['get_all_kabupaten'] = $this->Barzah_model->get_all_kabupaten();
    $this->data['get_all_kecamatan'] = $this->Barzah_model->get_all_kecamatan();
    $this->data['get_all_kelurahan'] = $this->Barzah_model->get_all_kelurahan();
    $this->data['individu']          = $this->Barzah_model->get_all_admin_individu();
    $this->data['komunitas']         = $this->Barzah_model->get_laporan_komunitas();

    $data['path'] = base_url('assets');
    $this->load->view('back/barzah/laporan/pusat', $this->data);
  }

  function laporan_admin()
  {
    is_login();
    is_read();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['page_title']        = 'Data ' . $this->data['module'];
    $this->data['header']            = 'Laporan Pusat';
    $this->data['get_all_provinsi']  = $this->Barzah_model->get_all_provinsi();
    $this->data['get_all_kabupaten'] = $this->Barzah_model->get_all_kabupaten();
    $this->data['get_all_kecamatan'] = $this->Barzah_model->get_all_kecamatan();
    $this->data['get_all_kelurahan'] = $this->Barzah_model->get_all_kelurahan();
    $this->data['individu']          = $this->Barzah_model->get_all_admin_individu();
    $this->data['umur'] = [];
    for ($i = 0; $i < count($this->data['individu']); $i++) {
      $umur = $this->data['individu'][$i]->tgl_lahir;
      $res_umur = $this->hitung_umur($umur);
      array_push($this->data['umur'], $res_umur);
    }
    $this->data['komunitas']         = $this->Barzah_model->get_laporan_komunitas();

    $data['path'] = base_url('assets');
    $this->load->view('back/barzah/laporan/admin', $this->data);
  }

  function hitung_umur($tanggal_lahir)
  {
    $birthDate = new DateTime($tanggal_lahir);
    $today = new DateTime("today");
    if ($birthDate > $today) {
      exit("0 tahun 0 bulan 0 hari");
    }
    $y = $today->diff($birthDate)->y;
    $m = $today->diff($birthDate)->m;
    $d = $today->diff($birthDate)->d;
    return $y . " tahun " . $m . " bulan " . $d . " hari";
  }

  //------------------------[ DETAIL LAPORAN INDIVIDU & KOMUNITAS ]-----------------------------//
  function detail_laporan_individu($id)
  {
    is_login();
    is_update();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['get_all']      = $this->Barzah_model->get_laporan_individu_detail($id);

    if ($this->data['get_all']) {
      $this->data['page_title'] = 'Detail Data ' . $this->data['module'];
      $this->load->view('back/barzah/laporan/detail_laporan_individu', $this->data);
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">Penduduk not found</div>');
      redirect('barzah');
    }
  }

  // -------------- DETAIL LAPORAN KOMNITAS --------------
  function detail_laporan_komunitas($id)
  {
    is_login();
    is_update();

    if (!is_superadmin()) {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">You can\'t access last page</div>');
      redirect('dashboard');
    }

    $this->data['get_all']      = $this->Barzah_model->get_laporan_komunitas_detail($id);

    if ($this->data['get_all']) {
      $this->data['page_title'] = 'Detail Data ' . $this->data['module'];
      $this->load->view('back/barzah/laporan/detail_laporan_komunitas', $this->data);
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger">Penduduk not found</div>');
      redirect('barzah');
    }
  }
}
