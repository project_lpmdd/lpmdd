<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class AjaxApi extends CI_Controller
{
    public function getKabupaten()
    {
        $this->load->model('Penduduk_model', 'Penduduk_model');
        $kabupaten = $this->Penduduk_model->get_kabupaten($_GET);
        echo json_encode($kabupaten);
    }
    public function getKecamatan()
    {
        $this->load->model('Penduduk_model', 'Penduduk_model');
        $kecamatan = $this->Penduduk_model->get_kecamatan($_GET);
        echo json_encode($kecamatan);
    }
    public function getDesa_kelurahan()
    {
        $this->load->model('Penduduk_model', 'Penduduk_model');
        $desa_kelurahan = $this->Penduduk_model->get_desa_kelurahan($_GET);
        echo json_encode($desa_kelurahan);
    }
    
}